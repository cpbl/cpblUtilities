#!/usr/bin/env python3
#Author: cpbl
"""

SGP singapore is missing from the get_world_map data!
"""
import docopt
import pandas as pd
import os
import re
import numpy as np
import matplotlib.pyplot as plt
from  cpblUtilities import defaults
paths=defaults['paths']

import geopandas as gpd

class geopandas_world_map(object):
    def __init__(self, db=None):
        """ db is a Dataframe of the master database
        Column specifications: ??
        """
        self.world = None  # The GIS map (countries)
        self.use_db(db)       # The WB database (indicators)
        self.color_lookup=None
        self.proj="ESRI:54009"

    def use_db(self, db):
        self.db = db
        
    def get_world_map(self):
        if self.world is not None: return self.world.copy()
        world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
        # 2021: N.B. Singapore is missing on purpose in this low-res dataset (https://github.com/nvkelso/natural-earth-vector/issues/345)
        # So we will need to ignore any SGP in plotting data (or handle it as a point)
        
        # Fix bugs in this dataset !!!!!  [fixed in 2021-11 or so. remove this?]  See ... ?
        world.loc[world['name'] == 'France', 'iso_a3'] = 'FRA'
        world.loc[world['name'] == 'Norway', 'iso_a3'] = 'NOR'
        world.loc[world['name'] == 'Somaliland', 'iso_a3'] = 'SOM'
        world.loc[world['name'] == 'Kosovo', 'iso_a3'] = 'RKS'
        world = world[(world.pop_est>0) & (world.name!="Antarctica")]

        if 1: # Bug: https://stackoverflow.com/questions/69512027/remove-hawaii-in-geopandas-naturalearth-lowres-world-map
            #world['L'] = world.geometry.map(lambda row: row.centroid.coords[0][0])
            
            # Create a custom polygon
            from shapely.geometry import Polygon        
            polygon = Polygon([(-175, -86), (-175, 85), (179, 85), (-175, 85), (-175, -86)])
            from shapely.geometry import box

            polygon = box(-150, -85, 175, 85)
            poly_gdf = gpd.GeoDataFrame([1], geometry=[polygon], crs=world.crs)

            world = gpd.clip(world,polygon)


        world = world.to_crs(self.proj)
        
        
        self.world = world.set_index('iso_a3')
        
        return self.world.copy()

    def get_color_mapper(self,data=None):
        if  self.color_lookup is None and data is  None: return None
        
        if  self.color_lookup is not None and data is  None:
            return self.color_lookup
        
        self.set_color_mapper(data)
        return self.color_lookup
    
    def set_color_mapper(self,data, colormap=None):
        if colormap is None: colormap = 'Oranges'
        from cpblUtilities.color import    assignColormapEvenly
        
        cme = assignColormapEvenly(colormap, data)
        #Oh no bug: this is producing negative value of e-17 !!! fix with wrapper:
        def cme_fix_infinitesimal_negative_values(x,cme1):
            return [max(0,xx) for xx in cme1(x)]
        
        self.color_lookup = lambda x: cme_fix_infinitesimal_negative_values(x,cme)
        
        return self.color_lookup        
        
        
    def plot_one_geopandas_map(self, dataseries=None,
                               savename =None,
                               proj = None,#"ESRI:54009",
                               cme =None,
                               #missing_kwds = None, # how to paint missing regions
                               missing_color=None
                               ):
        """
        If you want a log-scaled colorbar, plot log values and then mess with the yticks on cbax, which is returned.
        """
        """
        for proj in ("ESRI:54009", 'EPSG:4326'):
        """
        if dataseries is None:
            # Use self.db somehow? not done. This was mandatory in whr processWB version.
            dataseries = self.db.copy()
        if 0 and missing_kwds is None:
            missing_kwds = dict(color = "lightgrey",) 
        if missing_color is None:
            missing_color = "lightgrey"
        if not isinstance( missing_color,str):
            raise ValueError('Sorry; missing_color should be a hex string')
            
        proj = self.proj if proj is None else proj
        
        world=  self.get_world_map()

        if 'SGP' in dataseries:
            pass#print("\nWARNING: Dropping Singapore: it's too small for low-res world map\n")
            #dataseries = dataseries.drop('SGP')

        world = world.join(dataseries.to_frame('data'))
        # Leave others as NaN? or change to 0??  See missing_kwds: this should be used to paint NaN regions 


        print(' The world map from geopandas is missing an ISO code for:\n ', world.loc['-99'])

        #cities = gpd.read_file(gpd.datasets.get_path('naturalearth_cities'))

        from cpblUtilities.color import    assignColormapEvenly,addColorbarNonImage


        if cme is None:
            cme= self.get_color_mapper()
            if cme is None:
                cme=self.set_color_mapper(world.data)


        world['color'] = world.data.map(cme)
        # No; we want missing data regions to remain NaN, so missing_kwds is used:
        if 1:
            world['color'] = world['color'].where(world.data.notnull(), other=missing_color)
            #'missing_color')
            #wtf.replace('missing_color',missing_color)### Ahhh fails if missing_color is a tuple!

        else:
            assert world.color.dropna().map(np.min).min() >=0.0
            assert world.color.dropna().map(np.max).max() <=1.0


            assert world[pd.isnull(world.color)].empty
        print('   Following regions are missing from the world-coverage list:\n',
              world[world.apply(lambda row: row['color']==[0,0,0], axis=1)]  )

        #world = world.dropna(subset=['data']) # Nope; see missing_kwds for painting NaN regions
        plt.close('all')


        #fig,axs= plt.subplots(2,2)
        fig,ax= plt.subplots(1,)# facecolor=facecolor)

        h=world.to_crs(proj).plot(ax=ax, color=world.color,#column = 'all',
                                legend=True,
                                #legend_kwds={'label': "Population by Country",
                                #             'orientation': "horizontal"},
                                #cmap='OrRd',
                                categorical=False, # Is this doing nothing?
                                scheme='quantiles', #classification_
                                  #facecolor='b',
                                  #missing_kwds = missing_kwds,
                                )

        # Fix the axes to the whole world-ish:
        ax.set_xlim((-11969376.024047954, 15651081.336065887))
        ax.set_ylim((-5887433.195560829, 9423784.312780283))

        fcb = world.dropna(subset=['data'])
        
        #from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        cbax = ax.inset_axes([.88, .5, .03, .45],
                             #width="5%",  # width = 5% of parent_bbox width
                   #height="30%",  # height : 50%
                          #loc='upper right',
                          #bbox_to_anchor=(.9, .5, .05, .3),
                   transform=ax.transAxes,
                   #borderpad=0,
                   )

        
        hcb= addColorbarNonImage(dict(zip(fcb['data'], fcb['color'])),  cbax=cbax)
        #cbax=plt.gca()
        # [i.set_linewidth(0.1) for i in cbax.spines] Fails!
        if 1:
            pos1 = cbax.get_position() # get the original position
            # For a small plot:
            # pos2 = [pos1.x0-0.05, pos1.y0 +.3,  pos1.width, pos1.height*1/3]
            # for a larger one:
            pos2 = [pos1.x0-0.08, pos1.y0 +.4,  pos1.width+.1, pos1.height*2/8]
            # For a larger one:
            cbax.tick_params(axis='y', which='major', labelsize=9)
            plt.savefig('tmp.pdf')
            l, b, w, h = cbax.get_position().bounds
            #cbax.set_position([l-.2,]) # set a new position
            plt.savefig('tmp2.pdf')
        ax.set_axis_off()

        if savename:
            plt.savefig(savename)
        
        return (fig,ax,cbax)
