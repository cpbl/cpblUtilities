#!/usr/bin/python
"""
Various tools for use with OSM and other country indices / matching.

Also see cpbl's whr2017 for some WHR-related matching.


2022: should really update cname2iso, etc with values from:
  import geopandas as gpd
  world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))


"""
import os, sys #, platform, time, psutil
import pandas as pd
import numpy as np
#from .cpblUtilities_config import paths
from ..cpblUtilities_config import defaults
paths = defaults['paths']

__local_input_path__ = os.path.dirname(__file__)+'/' 
class country_tools():
    def __init__(self, forceUpdate=False):
        """
        allc = pd.read_csv(paths['otherinput']+'CountryData/raw.githubusercontent.com_lukes_ISO-3166-Countries-with-Regional-Codes_master_all_all.csv')

         # https://raw.githubusercontent.com/datasets/country-codes/master/data/country-codes.csv
         # https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv ie https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv, now in CountryData as raw.githubusercontent.com_lukes_ISO-3166-Countries-with-Regional-Codes_master_all_all.csv

        """
        self.forceUpdate=forceUpdate
        SP='/tmp/' # paths['scratch']
        self.useful_sites={'lukes':{'tmpfile':SP+'countryTools_lukes_tmp.pandas', 'url': 'https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv',
                                    'rename':{'region':'region5', 'sub-region':'region22', 'name':'countryName'}},
                           # N.B. The following includes six languages for all countries,  not every country's official language
                     'multilang':{'tmpfile':SP+'countryTools_gitdatasets_tmp.pandas', 'url': 'https://raw.githubusercontent.com/datasets/country-codes/master/data/country-codes.csv',
                                  'rename':{}}}

        return
    @staticmethod
    def country2ISOLookup():
        """
        Returns dict of dicts
        cname2ISO is country name to ISO alpha 3 lookup
        ISO2cname is the reverse
        ISO2shortName is a consistent dict of short names (they might not be officials)
            which also strip out non-ASCII128 characters (sorry, Cote d'Ivoire...)

        This could easily be adapted to return WB codes as well
        """
        import pandas as pd
        lookup = pd.read_table(os.path.dirname(__file__)+'/nationsonline.org.tsv',         # Are these OSM-oriented??
                               dtype={'ISO3digit=UN-M49-Numerical':'str'},
                               encoding='utf8')
        lookup['ISO3digit'] =lookup['ISO3digit=UN-M49-Numerical']
        oldCPBLlookup_unused_here_so_far = pd.read_table(os.path.dirname(__file__)+'/countrycode_main.tsv',
                               dtype={'ISO3digit':'str'},
                               skiprows =3)
        # The following, since we append them to the end, probably has the effect of overwriting country names to these simpler ones in ISO2cname, etc.
        # Another way to do this would be to keep creating new columns of alternative names, and then when doing a name index, use them all, but otherwise have a preferred one (the shortest!?)
        ## 2022-06: I'm not sure I fully understand that. I'm now making a repeated use of this equivalents list below, just for the cname2ISO lookup.
        equivalents = list(zip(*[LL.strip().split('\t') for LL in """
            Hong Kong, SAR China	Hong Kong
            Macao, SAR China	Macao
            Iran, Islamic Republic of	Iran
            Macedonia, Republic of	Macedonia
            Micronesia, Federated States of	Micronesia
            Taiwan, Republic of China	Taiwan
            Tanzania, United Republic of	Tanzania
            Korea (North)	North Korea
            Korea (South)	South Korea
            Korea	South Korea
            Palestinian Territory	Palestine
            Russian Federation	Russia
            Syrian Arab Republic (Syria)	Syria
            Slovak Republic	Slovakia
            United Kingdom	Great Britain
            Venezuela (Bolivarian Republic)	Venezuela
            Bosnia Herzegovina	Bosnia and Herzegovina
            Czech Rep.	Czech Republic
            Dominican Rep.	Dominican Republic
            Hong Kong SAR	Hong Kong
            Macau SAR	Macao
            North Macedonia	Macedonia
            Taiwan ROC	Taiwan
            """.strip().split('\n')]))
        eq=pd.DataFrame({'countryname': equivalents[0], 'newname':equivalents[1]})
        edf = lookup[lookup.countryname.isin(eq.countryname)].merge(eq)
        edf['countryname']= edf['newname']

        lookup = pd.concat([lookup, edf[lookup.columns] ] )

        cname2ISO = lookup.set_index('countryname').ISOalpha3.to_dict()

        ISO2cname = lookup.set_index('ISOalpha3').countryname.to_dict()
        ISO2cname['ALL'] = 'World'
        ISOalpha2ISOdigit = lookup.set_index('ISOalpha3').ISO3digit.to_dict()
        ISOdigit2ISOalpha = lookup.set_index('ISO3digit').ISOalpha3.to_dict()

        lookup = pd.read_table(os.path.dirname(__file__)+'/shortNames.tsv', dtype={'ISO3digit':'str'})
        ISOalpha2shortName = lookup.set_index('ISOalpha3').shortName.to_dict()
        ISOalpha2shortName['ALL'] = 'World'


        # Add in some alternate names by name variants (defined above as "equivalents")
        for k,v in dict(zip(equivalents[0], equivalents[1])).items():
            if v in cname2ISO:
                if k  in cname2ISO:
                    print('  country_tools.country2ISOLookup: Not merging alternative "{}" because it is already known'.format(k))
                else:
                    cname2ISO[k]= cname2ISO[v]
            else:
                print('  Cannot merge ',k,v)
        
        # Add in some alternate names manually (different variants of country name)
        # these are only used in going TO iso from the country name
        # We could also do some automatically, based on country names with parentheses in them.
        # MOST OF THESE SHOULD BE ADDED INSTEAD AS EQUIVALENTS, ABOVE.
        cname2ISO.update({'The Bahamas': 'BHS', 'United Republic of Tanzania': 'TZA', 'Ivory Coast': 'CIV', 
                          'Republic of Serbia': 'SRB', 'Guinea Bissau': 'GNB', 'Iran': 'IRN', 'Democratic Republic of the Congo': 'COD',
                          'Republic of Congo': 'COG',  'Bolivia': 'BOL', 'South Korea': 'KOR', 'Laos': 'LAO',
                          'Brunei': 'BRN', 'East Timor': 'TLS', 'Vietnam': 'VNM',  'North Korea': 'PRK', 'Moldova': 'MDA', 'Vatican City': 'VAT',
                          'Macedonia': 'MKD', 'United Kingdom': 'GBR', 'Tanzania':'TZA', 'Cape Verde':'CPV', 'Reunion':'REU', 'Falkland Islands':'FLK', 
                          'Micronesia':'FSM', 'United States':'USA', 'USA':'USA',
                           'PEOPLES R CHINA':'CHN',
                          'U ARAB EMIRATES':'UAE',
                          })
        for kk,vv in list(cname2ISO.items()): cname2ISO[kk.lower()] = vv
        
        
        return {'cname2ISO':cname2ISO, 'ISO2cname': ISO2cname, 'ISOalpha2ISOdigit': ISOalpha2ISOdigit, 'ISOdigit2ISOalpha': ISOdigit2ISOalpha, 'ISOalpha2shortName':ISOalpha2shortName }
    
    def update_downloads(self):
        for kk,dd in list(self.useful_sites.items()):
            if self.forceUpdate or not os.path.exists(dd['tmpfile']):
                # Keep Pandas from translating Namibia's "NA" to NaN:
                tdf = pd.read_csv(dd['url'],na_filter = False).to_pickle(dd['tmpfile']) #, encoding='utf8'
                print(('   Loaded '+ dd['url']))
    def load_and_rename_cols(self,key):
        """ Load one of the files in useful_sites, and apply the column renamings
        """
        self.update_downloads()
        df = pd.read_pickle(self.useful_sites[key]['tmpfile'])
        df.rename(columns = self.useful_sites[key]['rename'], inplace=True)
        return df
        
    def get_country_list(self, columns=None):
        """ columns can be used to imply which info you need. See the useful_sites "rename"s for column names
        """
        df = self.load_and_rename_cols('lukes')
        assert df[pd.isnull(df.region5)].empty
        ###df = df[pd.notnull(df.region)]
        df['iso2'] =df['iso_3166-2'].str[-2:].values
        df['ISOalpha3'] =df['alpha-3']
        if columns is not None:
            assert all([cc in df for cc in columns])
            df=df[columns]

        #df2=self.load_and_rename_cols('multilang')
        return df

    @staticmethod
    def get_Gallup_country_lookups(verbose=True):
        this = country_tools.get_Gallup_country_lookups
        if "_Gallup_country_lookups" in this.__dict__:
            return this._Gallup_country_lookups

        """ Kosovo is the only GWP country not matched to a 3-letter ISO code. Let's ignore it.
        """
        dfr = pd.read_table(__local_input_path__+'GallupWorldPoll-region-country.tsv').rename(columns={'country':'rcountry'})
        dfr['lccountry'] = dfr.rcountry.str.lower()
        dfr = dfr.set_index('lccountry')
        dfw = pd.read_table(__local_input_path__+'GallupWorldPoll-WP5-defs-2016.tsv').rename(columns={'country':'wcountry'})
        dfw['lccountry'] = dfw.wcountry.str.lower()
        dfw = dfw.set_index('lccountry')
        wp5s = pd.read_table(__local_input_path__ +'countrycode_main.tsv',  skiprows=3).set_index('country_GWP3_wp5')
        wp5s = wp5s[['countryCode_GWP3_wp5', 'countryCode_ISO3','country_bestShortName','country_bestName','twoletter_AlexShultz_svg']]
        df= wp5s.join(dfr).join(dfw).rename(columns = {'countryCode_ISO3':'ISO',})
        df.index.name = 'country'
        assert 'South Africa'.lower() in dfr.rcountry
        assert 'South Africa'.lower() in df.index


        # Now several checks:
        # Did regions get their ISO?
        problems = {
            ' Published WHR country lacks an ISO: ': df[pd.notnull(df.rcountry) & pd.isnull(df.ISO)][['ISO','countryCode_GWP3_wp5','WP5','rcountry']],
            ' Published WHR country lacks a WP5: ': df[pd.notnull(df.rcountry) & pd.isnull(df.WP5)],
            ' Published WHR country lacks a map code: ': df[pd.notnull(df.rcountry) & pd.isnull(df.twoletter_AlexShultz_svg)],
            ' Old Gallup micro country lacks an ISO in my master lookup: ': df[pd.notnull(df.countryCode_GWP3_wp5) & pd.isnull(df.ISO)][['ISO','countryCode_GWP3_wp5','WP5','wcountry']],
            ' 2016 Gallup micro country lacks an ISO in my master lookup: ': df[pd.notnull(df.WP5) & pd.isnull(df.ISO)][['ISO','countryCode_GWP3_wp5','WP5','wcountry']],
        }

        if verbose:
            for tt,dd in list(problems.items()):
                if not dd.empty:
                    print(('\n\n -- country_tools WARNING: '+tt))
                    print(dd)
        this._Gallup_country_lookups = df.reset_index()
        return df.reset_index()

def loadOtherCountryData(forceUpdate=False, imputation=True):
    """
    Loads dataframe of other variables at the country level
    Returns a dataframe (not indexed, but columns  have ISO and WB codes)
    Complicating factor is that World Bank alpha codes are almost but not quite the same as ISO alpha codes...be careful!
    Romania is the easy way to see this (ROU in ISO, ROM in WB)
    NOTE: in 2016, the WB changed its country codes to align more with the ISO codes

NB There's another nearly-identical version in bin/osm/globalSprawl
    """
    import pandas as pd

    dataFn = paths['scratch'] + 'otherCountryData.pandas'
    if os.path.exists(dataFn) and not forceUpdate:
        df = pd.read_pickle(dataFn)
        print(' loadOtherCountryData: using saved data.')
        return df

    print("(Re)creating other country data file")
    DP = __local_input_path__+'/'  # data path for raw data files

    df = pd.read_table(DP+'countrycodes.tsv', dtype={'ISO3digit':'str'})
    df.ISO3digit = df.ISO3digit.str.zfill(3)
    df.set_index('ISO3digit', inplace=True)
    ncountries = len(df)
    print(' Remaining other country data not loaded in cpblUtilities. See gallup / macro / sprawl repos for more')
    return df # ------------------------================================== REST OF THIS METHOD IGNORED in CPBL_UTILITIES!
    # Some are indexed by ISO3digit
    for fn in ['fuelprices2014.tsv', ]:
        newDf = pd.read_table(DP + fn, dtype={'ISO3digit':'str'}).set_index('ISO3digit')
        if 'ISOalpha3' in newDf.columns: newDf.rename(columns={'ISOalpha3':'ISOalpha3new'}, inplace=True)
        df = df.combine_first(newDf) # will update country and add new columns
        if 'ISOalpha3new' in newDf.columns: # verify that the ISO codes match
            assert all((df.ISOalpha3==df.ISOalpha3new) | pd.isnull(df.ISOalpha3) | pd.isnull(df.ISOalpha3new))
            df.drop('ISOalpha3new', axis=1, inplace=True)
            assert ncountries == len(df)  # make sure no new countries have been added!

    # From here on, data is indexed by ISOalpha3
    df.reset_index(inplace=True)
    df.set_index('ISOalpha3', inplace=True)

    for fn in ['CO2_2014.tsv','oilreserves.tsv','internetUsers.tsv']:
        newDf = pd.read_table(DP + fn, encoding='latin_1').set_index('ISOalpha3')
        if 'pop2014_millions' in newDf.columns: newDf.rename(columns={'pop2014_millions':'pop2014_millions_IEA'}, inplace=True)
        df = df.combine_first(newDf)
        assert ncountries == len(df)  # make sure no new countries have been added!

    # rugged is in csv format
    newDf = pd.read_csv(DP + 'rugged_data.csv', encoding='latin_1').set_index('isocode')
    newDf = newDf[newDf.country!='Serbia and Montenegro']   # this is not a current country!
    newDf = newDf[['rugged', 'rugged_popw', 'rugged_slope', 'rugged_lsd', 'rugged_pc']]
    df = df.combine_first(newDf)
    assert ncountries == len(df)

    # Road length data
    #newDf = pd.read_csv(DP + 'CIA_roadlength.tsv', sep='\t').set_index('ISOalpha3') # old version, from CIA World Factbook
    newDf = pd.read_csv(DP + 'IRF_2014_World_Road_Statistics.tsv', sep='\t', encoding='latin_1').set_index('ISOalpha3') # entered from IRF World Road Statistics
    df = df.combine_first(newDf[['roads_datayear','roads_km','roads_pc_paved','roads_excludes_urban_or_local']])

    # From here on, data is indexed by WBcode
    assert df.index.name=='ISOalpha3' or df.index.name is None
    df.index.name='ISOalpha3' # this got lost
    df.reset_index(inplace=True)
    df.set_index('WBcode2015', inplace=True)

    for fn in ['governance.tsv']:
        newDf = pd.read_table(DP + fn, encoding='latin_1').set_index('WBcode')
        newDf.index.name='WBcode2015'
        df = df.combine_first(newDf)
        assert ncountries == len(df)  # make sure no new countries have been added!

    # WDI data is special - in CSV, and also we want a subset of the data
    WDIdf = pd.read_csv(DP + 'WDIData.csv')

    # drop non-countries
    countriesToDrop = ['AFE', 'AFW', 'XKX','ARB', 'CSS', 'CEB', 'CHI', 'EAR','EAS', 'EAP', 'EMU', 'ECS', 'ECA', 'EUU', 'FCS', 'HPC', 'HIC', 'IBD','IBT','IDA','IDB','IDX','LTE','NOC', 'OEC', 'LCN', 'LAC',
                       'LDC', 'LMY', 'LIC', 'LMC', 'MEA', 'MNA', 'MIC', 'NAC', 'INX', 'OED', 'OSS', 'PRE','PSS','PST', 'SST', 'SAS', 'SSF', 'SSA', 'TEA','TLA','TEC','TMN','TSA','TSS','UMC', 'WLD']
    WDIdf['keep'] = WDIdf['Country Code'].apply(lambda x: False if x in countriesToDrop else True)
    WDIdf = WDIdf[WDIdf.keep]

    WDIyear = '2021'
    df.reset_index(inplace=True)
    df.set_index('WBcode', inplace=True)
    # note - electricity/energy indicators don't work with latest update. See #619 in OSM repo
    # they need to be updated with new column names
    WDIindicators = [('Birth rate, crude (per 1,000 people)', 'crudeBirthRate'), ('Death rate, crude (per 1,000 people)', 'crudeDeathRate'),
                     ('Electricity production (kWh)', 'kWhelec'),
                     ('Electricity production from coal sources (kWh)', 'kWhCoal'), ('Electricity production from hydroelectric sources (kWh)', 'kWhHydro'),
                     ('Electricity production from natural gas sources (kWh)', 'kWhNatGas'), ('Electricity production from nuclear sources (kWh)', 'kWhNuclear'),
                     ('Electricity production from oil sources (kWh)', 'kWhOil'), ('Electricity production from renewable sources (kWh)', 'kWhRenewable'),
                     ('Energy imports, net (% of energy use)', 'energyImportsPc'), ('Energy production (kt of oil equivalent)', 'energyProduction_ktoe'),
                     ('Energy use (kg of oil equivalent per capita)', 'energyUse_kgoe_pc'), ('Oil rents (% of GDP)', 'oilRents_pcofGDP'),
                     ('Pump price for diesel fuel (US$ per liter)', 'dieselUSDlitre_WB'), ('Pump price for gasoline (US$ per liter)', 'gasolineUSDlitre_WB'),
                     ('Rail lines (total route-km)', 'railRouteKm'), ('Railways, goods transported (million ton-km)', 'rail_Mtkm'),
                     ('Railways, passengers carried (million passenger-km)', 'rail_MpassKm'), ('Literacy rate, adult total (% of people ages 15 and above)', 'literacyPcAdult'),
                     ('Individuals using the Internet (% of population)', 'internetPer100'), ('Mobile cellular subscriptions (per 100 people)', 'mobilePhonesPer100'),
                     ('Urban population', 'urbanPop'+WDIyear), ('Urban population (% of total population)', 'urbanPopPc'+WDIyear), ('Urban population growth (annual %)', 'urbanPopPcGrowth'+WDIyear),
                     ('Population, total', 'pop'+WDIyear), ('Population density (people per sq. km of land area)', 'popDensity_sqkm'+WDIyear),
                     ('Population growth (annual %)', 'popPcGrowth'), ('Population in urban agglomerations of more than 1 million', 'popMegaCity'),
                     ('Land area (sq. km)', 'landSqKm'), ('Gini index (WB)', 'gini'),
                     ('Exports of goods and services (% of GDP)', 'exportsPc'), (f'GDP (constant 2017 US$)', 'gdp{WDIyear}USD_WB'),
                     (f'GDP per capita (constant 2017 US$)', f'gdpCapita{WDIyear}USD_WB'), ('GDP growth (annual %)', 'gdpGrowthPc_WB'),
                     ('GDP per capita, PPP (constant 2017 international $)', 'gdpCapitaPPP_WB'), ('Final consumption expenditure (constant 2005 US$)', 'fincons2005USD_WB'),
                     ('CPIA policy and institutions for environmental sustainability rating (1=low to 6=high)', 'sustPolicyRating')]


    for (indicator, colName) in WDIindicators:
        newDf = WDIdf[WDIdf['Indicator Name']==indicator][['Country Code', WDIyear]].set_index('Country Code')
        newDf.rename(columns={WDIyear: colName}, inplace=True)   # rename column to indicator name
        if len(newDf)==0: 
            print('Missing data for {}'.format(indicator))
            
        df = df.combine_first(newDf)    

        assert ncountries == len(df)  # make sure no new countries have been added!

    assert df.index.name=='WBcode' or df.index.name is None
    df.index.name='WBcode' # this got lost
    df.reset_index(inplace=True)

    notfloat=[[vv,df[vv].dtype] for vv in df.columns if str(df[vv].dtype) not in ['float64']]
    assert [vv in ['ISO3digit','WBcode','countryname'] for vv,dt in notfloat]

    """
    Impute GDP for missing countries

    Argentina, Myanmar and Somalia (and maybe some of the small countries like Andorra)
    have other measures of GDP available, just not the PPP version that we use.
    So we cam impute GDPCapitaPPP from the version at market exchange rates.
    """
    if imputation is True or (imputation not in [False,None] and  'gdpCapitaPPP_WB' in imputation):
        df['gdpCapita_GDP2014PPP_bn2005USD']=1e9*df['GDP2014PPP_bn2005USD']/df[f'pop{WDIyear}']
        # This one's simple: four four countries, we have an R^2=.99 scalar linear proxy
        x,y='gdpCapita_GDP2014PPP_bn2005USD','gdpCapitaPPP_WB'
        newvals,df2=df_impute_values_ols(df,y,x)
        print((' Added '+y+' to following countries:\n'+str(df.loc[newvals.index]['ISOalpha3'])))
        if not 'not verbose':
            dfm=df[df.ISOalpha3.map(lambda ss: ss in ['ARG', 'MMR', 'SOM', 'SYR', 'AND', 'LIE', 'MCO', 'SMR'])]
            gdpvars=[vv for vv in df.columns if vv.lower().startswith('gdp')]
            print(dfm[['ISOalpha3']+gdpvars])
            #df.set_index('gdpCapitaPPP_WB')[['gdpCapita2005USD_WB']].plot(kind='scatter')
            from cpblUtilities.mathgraph import cpblScatter
            cpblScatter(df,x,y,labels='ISOalpha3')
            for iso,arow in dfm.set_index('ISOalpha3').iterrows():
                plt.plot([arow[x],arow[x]],plt.gca().get_ylim())
                if pd.notnull(arow[x]):
                    plt.gca().text(arow[x],plt.gca().get_ylim()[0],iso)
        df=df2
    if imputation  is True or (imputation not in [False,None] and 'internetUsersper100_2013' in imputation):
        y='internetUsersper100_2013'
        newvals,df=df_impute_values_ols(df,y,' np.log(gdpCapitaPPP_WB) + GovernanceVoice2013 + np.log(pop2021) ')
        print((' Added '+y+' to following countries:\n'+str(df.loc[newvals.index]['ISOalpha3'])))

    df.to_pickle(dataFn)
    return df

def country2WBregionLookup():
    """
    Returns lookup of World Bank region to country (by ISOalpha3)
    Indexed by WB region code ('GroupCode')

    201711: I don't like the WB regions. Augment them with Gallup classification (or find others?)
        # Other resources:
        # https://raw.githubusercontent.com/datasets/country-codes/master/data/country-codes.csv
        # https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv ie https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv, now in CountryData as raw.githubusercontent.com_lukes_ISO-3166-Countries-with-Regional-Codes_master_all_all.csv
    """
    import pandas as pd
    WB2ISO = loadOtherCountryData()[['WBcode','WBcode2015','ISOalpha3']]
    WBregions = pd.read_csv(__local_input_path__+'WBregions.csv')
    WBregions = WBregions.merge(WB2ISO, left_on='CountryCode', right_on='WBcode2015').set_index('GroupCode')
    WBregions.drop('CountryCode', axis=1, inplace=True) # duplicate to WBcode
    topWBregions = ['World','Africa',
                          'European Union',
       'East Asia & Pacific (all income levels)',
                          'South Asia', 'North America' ,
       'Latin America & Caribbean (all income levels)',
         'Central Europe and the Baltics',
                          'High income',                          'Middle income',       'Low income',]
    # Put the above list first
    notIntop = list(set(WBregions.GroupName.unique())-set(topWBregions))
    lwb = len(WBregions)
    WBregions = WBregions.reset_index().set_index('GroupName').loc[topWBregions+notIntop].reset_index().set_index('GroupCode')
    assert len(WBregions) == lwb

    cl = country_tools().get_country_list()
    cl_missing = WBregions.loc[~(WBregions.ISOalpha3.isin(cl.ISOalpha3.values))] # Only Kosovo missing! :)
    cl = cl[cl.region5!='']
    assert not any([rr in cl.region22.values for rr in cl.region5.unique()])
    # Use existing names?? (or vice versa?)
    cl = cl.set_index('ISOalpha3').join(WBregions.reset_index().set_index('ISOalpha3')[['CountryName']]).reset_index()
    # Add in two more sets of regions. The GroupCode will be the same as its name
    cl5 = cl.rename(columns = {'region5':'GroupName'}).set_index('GroupName', drop=False)[['ISOalpha3', 'GroupName', 'CountryName']].drop_duplicates()
    cl22 = cl.rename(columns = {'region22':'GroupName'}).set_index('GroupName', drop=False)[['ISOalpha3', 'GroupName', 'CountryName']].drop_duplicates()

    # Use GADM lookup!? Nice definitions of continents, but not unique assignments to continents. Or did I get someone to code these by hand? No, that was maybe cities.

    # Use gallup? It's not very complete, but also nice definitions
    if 0:
        gallupRegions = pd.read_table(paths['otherinput']+'CountryData/gallup-region-country.tsv').set_index('country')
        wbr=WBregions[['ISOalpha3','CountryName']].set_index('CountryName').drop_duplicates()
        g2iso = gallupRegions.join(wbr)
        problemCountries = g2iso.loc[~(g2iso.index.isin(wbr.index))]
        problemCountries2 = wbr.loc[~(wbr.index.isin(g2iso.index))]
        # too hard for th emoment. Try something else:


    # So just Make some of our own regions, using the WB ones?
    afr = WBregions.query('GroupName=="Africa"').reset_index()#.ISOalpha3.values
    ssa = WBregions.loc['SSA'].reset_index()#.ISOalpha3.values
    mena = WBregions.query('GroupName=="Middle East & North Africa (all income levels)"').reset_index()#.ISOalpha3.values

    northAfr = afr[~(afr.ISOalpha3.isin(ssa.ISOalpha3.values))]
    # this produces setting with copy warning: northAfr['GroupName'] = 'North Africa'
    northAfr = northAfr.assign(GroupName = 'North Africa')
    northAfr = northAfr.assign(GroupCode = 'NorthAfrica') #    northAfr['GroupCode'] = 'NorthAfrica'
    mideast =  mena[~(mena.ISOalpha3.isin(afr.ISOalpha3.values))]
    mideast = mideast.assign(GroupName = 'Middle East') #mideast['GroupName'] = 'Middle East'
    mideast = mideast.assign(GroupCode = 'MiddleEast') #mideast['GroupCode'] = 'MiddleEast'

    # WBregions = pd.concat([WBregions, cl5, cl22])  # not yet used
    return WBregions
    
# See / integrate country2... in osmTools.py
"""
 country2ISOLookup(): could use multilang one! 
"""

################################################################################################
if __name__ == '__main__':
################################################################################################
    # Test everything
    cc = country_tools(forceUpdate = True)
    cc.get_country_list()
    
