#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Provide a DF to serve as lookup between ISO3, country names, and Inglehart cultural groups.
To use: 
from Inglehart_groups import Inglehart
"""

import pandas as pd
def hardcoded_Inglehart_data():
    missing =[
    u'Trinidad and Tobago',
    u'Armenia',
    u'Palestine',
    u'Lebanon',
    u'Libya',
    u'Yemen',
    u'Tunisia',
    u'Bahrain',
    u'Qatar',
    u'Kuwait',
    u'Georgia',
    u'Philippines',
    u'Singapore',
    u'Azerbaijan',
    u'Uzbekistan',
    u'Kyrgyzstan',
    u'Kazakhstan',
    ]

    Inglehart_groups_2008_retired_missing_ISO3 ={ # So this seems entirely redundant, given that I have a lookup including ISO3 below.
    #'2008 version', 
    'Orthodox':
        (
    'Russia', 
    'Bulgaria', 
    'Estonia', 
    'Ukraine', 
    'Belarus', 
    'Montenegro', 
    'Latvia', 
    'Albania', 
    'Serbia', 
    'Moldova', 
    'North Macedonia', 
'Bosnia Herzegovina',
    'Romania', 
            ),
    'Confucian':
    (
    'Japan', 
    'Taiwan', 
    'Hong Kong', 
    'China', 
    'South Korea', 
        #'S.Korea', 
    ),
    'Islamic':
        (
    'Iraq', 
    'Indonesia', 
    'Ethiopia', 
    'Turkey', 
    'Iran', 
    'Bangladesh',  # Wot! This is in two categories!
    'Pakistan', 
    'Jordan', 
    'Morocco', 
    'Egypt', # Wot! This should (also) be in AFrica
    'Algeria', 
        ),
    'Africa': # This misses many! that are in Islamic 
    (
    'Zambia', 
    'Zimbabwe', 
    'Uganda', 
    'Mali', 
    'Burkina Faso', 
    'Rwanda', 
    'Nigeria', 
    'South Africa', 
    'South Africa', 
    'Tanzania', 
    'Ghana',
    ), 
    'Latin America':
    (
    'Uruguay', 
    'Argentina', 
    'Brazil', 
    u'Ecuador',
    'Chile', 
    'Peru', 
    'Mexico', 
    'Guatemala', 
    'Venezuela', 
    'Colombia', 
    'Puerto Rico', 
    'El Salavador', 
    ), 
    'English speaking':
    (
    'Australia', 
        'United Kingdom', #Britain', 
    'New Zealand', 
    'Canada', 
    'North Ireland', 
    'Ireland', 
    'United States',
    #'U.S.A', 
    ), 
    'Catholic Europe':
    (
    'Slovakia', 
    'Slovenia', 
    'Czech Republic', 
    'Belgium', 
    'France', 
    'Luxembourg', 
    'Italy', 
    'Spain', 
    'Croatia', 
    'Poland', 
    ), 
    'Protestant Europe':
    (
    'Sweden', 
    'Germany', 
    'Norway', 
    'Denmark', 
    'Finland', 
    'Netherlands', 
    'Switzerland', 
    'Iceland', 
    ), 
    'South Asia':
    (
    'Vietnam', 
    'India', 
    'Cyprus', 
    'Thailand', 
    'Malaysia', 
    'Afghanistan', 
    'Bangladesh', 
    'Bhutan', 
    'India', 
    'Maldives', 
    'Nepal', 
    'Pakistan', 
    'Sri Lanka', 
    )
        }


    #Assign some coujntries not in WVS to Inglehart regions?
    # cpbl has coded the following by inspection / reference to religious breakdown in country / etc.  Many need checking or are unknown
    # In principal, we cannot assign unclear countries without fielding the WVS questions in those countries.
    shortnames = dict(I=  'Islamic', SA='South Asia', CE='Catholic Europe', O='Orthodox',
                      C='Confucian', LA='Latin America', A='Africa', E= 'English speaking', PE=  'Protestant Europe')

    Inglehart_GWP_not_in_WVS=[ # ISO3, country, Inglehart2008
           ['SAU', 'Saudi Arabia', 'Islamic' ],
           ['HUN', 'Hungary', 'Protestant Europe' ], #?
           ['SGP', 'Singapore',  ],  #?
           ['ISR', 'Israel',  ],  #??
           ['BEN', 'Benin', 'Africa', ],
           ['MDG', 'Madagascar', 'Africa', ],
           ['MWI', 'Malawi', 'Africa', ],
           ['PHL', 'Philippines', 'South Asia' ],
           ['MOZ', 'Mozambique', 'Africa', ],
           ['NER', 'Niger', 'Africa', ],
           ['SEN', 'Senegal', 'Africa',  ], #??
           ['GEO', 'Georgia', 'Orthodox' ],
           ['KGZ', 'Kyrgyzstan',  'Islamic'], #?
           ['CMR', 'Cameroon', 'Africa', ],
           ['SLE', 'Sierra Leone', 'Africa', ],
           ['ARM', 'Armenia',  ], #????
           ['AZE', 'Azerbaijan',  'Islamic' ], # ???
           ['BOL', 'Bolivia', 'Latin America' ],
           ['BIH', 'Bosnia Herzegovina', 'Orthodox' ],
           ['TCD', 'Chad', 'Africa', ],  #??
           ['CUB', 'Cuba', 'Latin America' ],
           ['DOM', 'Dominican Republic', 'Latin America'  ],
           ['SLV', 'El Salvador', 'Latin America' ],
           ['HTI', 'Haiti', 'Latin America' ],
           ['JAM', 'Jamaica',  ], #????
           ['LTU', 'Lithuania',  ], #??
           ['MKD', 'North Macedonia', 'Orthodox', ],
           ['PRT', 'Portugal', 'Catholic Europe' ],
           ['PRI', 'Puerto Rico', 'Latin America' ], # ???
           ['SRB', 'Serbia', 'Orthodox' ],
           ['TGO', 'Togo',  'Africa',],
           ['TTO', 'Trinidad and Tobago',  ],   #???
           ['ARE', 'United Arab Emirates', 'Islamic' ],
           ['UZB', 'Uzbekistan', 'Islamic' ], #??
           ['unknown', 'Kosovo',  ],
           ['GBR', 'United Kingdom', 'English speaking' ],
           ['GRC', 'Greece', 'Orthodox' ],
           ['KHM', 'Cambodia', 'South Asia' ],
           ['LAO', 'Laos', 'South Asia' ], #?
           ['MRT', 'Mauritania','Africa',  ],#???
           ['KAZ', 'Kazakhstan', 'Islamic' ], #???
           ['CRI', 'Costa Rica', 'Latin America' ],
           ['BLZ', 'Belize', 'Latin America' ],
           ['CAF', 'Central African Republic',  ],
           ['GUY', 'Guyana', 'Latin America'  ],
           ['HND', 'Honduras', 'Latin America' ],
           ['LBR', 'Liberia', 'Africa', ],
           ['NAM', 'Namibia', 'Africa', ],
           ['NIC', 'Nicaragua', 'Latin America' ],
           ['PAN', 'Panama', 'Latin America'  ],
           ['TJK', 'Tajikistan', 'Islamic' ], #?
           ['LBN', 'Lebanon', 'Islamic' ],
           ['SYR', 'Syria', 'Islamic' ], #???
           ['KEN', 'Kenya', 'Africa', ],
           ['PSE', 'Palestine', 'Islamic' ],
           ['BWA', 'Botswana', 'Africa', ],
           ['BFA', 'Burkina Faso', 'Africa', ],
           ['AUT', 'Austria', 'Protestant Europe', ],
           ['BDI', 'Burundi', 'Africa', ],
           ['unknown', 'Congo Brazzaville', 'Africa', ],
           ['DJI', 'Djibouti', 'Africa', ],
           ['MLT', 'Malta',  ], #??
           ['MNG', 'Mongolia', 'Confucian' ],
           ['QAT', 'Qatar',  'Islamic' ],
           ['unknown', 'Congo Kinshasa', 'Africa', ],
           ['CIV', 'Ivory Coast', 'Africa', ],
           ['PRY', 'Paraguay', 'Latin America' ],
           ['TKM', 'Turkmenistan', 'Islamic' ],
           ['BHR', 'Bahrain', 'Islamic' ],
           ['COM', 'Comoros',  ],
           ['KWT', 'Kuwait', 'Islamic' ],
           ['SDN', 'Sudan', 'Africa', ],
           ['YEM', 'Yemen', 'Islamic' ],
           ['unknown', 'Somaliland', 'Africa', ],
           ['TUN', 'Tunisia', 'Africa', ],
           ['AGO', 'Angola', 'Africa', ],
           ['GAB', 'Gabon', 'Africa', ],
           ['GIN', 'Guinea', 'Africa', ],
           ['LSO', 'Lesotho','Africa',  ],
           ['MUS', 'Mauritius', 'unknown' ],  #???
           ['unknown', 'Eswatini',  ],
           ['OMN', 'Oman', 'Islamic' ],
           ['MMR', 'Myanmar', 'South Asia', ],
           ['LBY', 'Libya', 'Africa' ], 
           ['SUR', 'Suriname', 'Africa', ],
           ['unknown', 'Northern Cyprus',  ],
           ['unknown', 'Nagorno Karabakh',  ],
           ['SOM', 'Somalia', 'Africa', ],
           ['SSD', 'South Sudan', 'Africa', ],
           ['GMB', 'Gambia', 'Africa',],

            ]

    Inglehart_GWP_not_in_WVS= pd.DataFrame(Inglehart_GWP_not_in_WVS, columns=['ISO3', 'country', 'Inglehart2008'])


    # Obviously these should be merged here, using country-ISO3 mapping of the WVS lookup.

    # 2022-10: Combine WVS 2008 assignments with my hand-coded ones above (hopefully this is right, and fairly complete:)  150 non-NaN rows
    csv="""ISO3,country,Inglehart2008
SAU,Saudi Arabia,Islamic
TUR,Turkey,Islamic
PAK,Pakistan,South Asia
IDN,Indonesia,Islamic
BGD,Bangladesh,South Asia
POL,Poland,Catholic Europe
HUN,Hungary,Protestant Europe
CZE,Czech Republic,Catholic Europe
ROU,Romania,Orthodox
IRN,Iran,Islamic
HKG,Hong Kong,Confucian
SGP,Singapore,
CHN,China,Confucian
IND,India,South Asia
BRA,Brazil,Latin America
NGA,Nigeria,Africa
ISR,Israel,
GHA,Ghana,Africa
BEN,Benin,Africa
MDG,Madagascar,Africa
MWI,Malawi,Africa
ZAF,South Africa,Africa
CAN,Canada,English speaking
AUS,Australia,English speaking
PHL,Philippines,South Asia
NZL,New Zealand,English speaking
MLI,Mali,Africa
MOZ,Mozambique,Africa
NER,Niger,Africa
RWA,Rwanda,Africa
SEN,Senegal,Africa
ZMB,Zambia,Africa
KOR,South Korea,Confucian
BLR,Belarus,Orthodox
GEO,Georgia,Orthodox
KGZ,Kyrgyzstan,Islamic
MDA,Moldova,Orthodox
RUS,Russia,Orthodox
UKR,Ukraine,Orthodox
CMR,Cameroon,Africa
SLE,Sierra Leone,Africa
ZWE,Zimbabwe,Africa
ALB,Albania,Orthodox
ARG,Argentina,Latin America
ARM,Armenia,
AZE,Azerbaijan,Islamic
BOL,Bolivia,Latin America
BIH,Bosnia Herzegovina,Orthodox
BGR,Bulgaria,Orthodox
TCD,Chad,Africa
COL,Colombia,Latin America
HRV,Croatia,Catholic Europe
CUB,Cuba,Latin America
CYP,Cyprus,South Asia
DOM,Dominican Republic,Latin America
SLV,El Salvador,Latin America
EST,Estonia,Orthodox
HTI,Haiti,Latin America
JAM,Jamaica,
LVA,Latvia,Orthodox
LTU,Lithuania,
MKD,North Macedonia,Orthodox
MYS,Malaysia,South Asia
MNE,Montenegro,Orthodox
NPL,Nepal,South Asia
PRT,Portugal,Catholic Europe
PRI,Puerto Rico,Latin America
SRB,Serbia,Orthodox
SVK,Slovakia,Catholic Europe
SVN,Slovenia,Catholic Europe
CHE,Switzerland,Protestant Europe
TGO,Togo,Africa
TTO,Trinidad and Tobago,
ARE,United Arab Emirates,Islamic
URY,Uruguay,Latin America
UZB,Uzbekistan,Islamic
unknown,Kosovo,
USA,United States,English speaking
GBR,United Kingdom,English speaking
DEU,Germany,Protestant Europe
BEL,Belgium,Catholic Europe
GRC,Greece,Orthodox
DNK,Denmark,Protestant Europe
JPN,Japan,Confucian
VEN,Venezuela,Latin America
LKA,Sri Lanka,South Asia
KHM,Cambodia,South Asia
LAO,Laos,South Asia
MRT,Mauritania,Africa
KAZ,Kazakhstan,Islamic
CRI,Costa Rica,Latin America
BLZ,Belize,Latin America
CAF,Central African Republic,
CHL,Chile,Latin America
ECU,Ecuador,Latin America
GTM,Guatemala,Latin America
GUY,Guyana,Latin America
HND,Honduras,Latin America
LBR,Liberia,Africa
NAM,Namibia,Africa
NIC,Nicaragua,Latin America
PAN,Panama,Latin America
PER,Peru,Latin America
TJK,Tajikistan,Islamic
EGY,Egypt,Islamic
LBN,Lebanon,Islamic
JOR,Jordan,Islamic
SYR,Syria,Islamic
FRA,France,Catholic Europe
NLD,Netherlands,Protestant Europe
ESP,Spain,Catholic Europe
ITA,Italy,Catholic Europe
SWE,Sweden,Protestant Europe
MEX,Mexico,Latin America
KEN,Kenya,Africa
TZA,Tanzania,Africa
PSE,Palestine,Islamic
UGA,Uganda,Africa
THA,Thailand,South Asia
BWA,Botswana,Africa
TWN,Taiwan,Confucian
AFG,Afghanistan,South Asia
BFA,Burkina Faso,Africa
AUT,Austria,Protestant Europe
BDI,Burundi,Africa
unknown,Congo Brazzaville,
DJI,Djibouti,Africa
FIN,Finland,Protestant Europe
ISL,Iceland,Protestant Europe
IRQ,Iraq,Islamic
IRL,Ireland,English speaking
LUX,Luxembourg,Catholic Europe
MLT,Malta,
MNG,Mongolia,Confucian
NOR,Norway,Protestant Europe
QAT,Qatar,Islamic
VNM,Vietnam,South Asia
unknown,Congo Kinshasa,
CIV,Ivory Coast,Africa
PRY,Paraguay,Latin America
TKM,Turkmenistan,Islamic
BHR,Bahrain,Islamic
COM,Comoros,
KWT,Kuwait,Islamic
SDN,Sudan,Africa
YEM,Yemen,Islamic
unknown,Somaliland,
TUN,Tunisia,Africa
MAR,Morocco,Islamic
DZA,Algeria,Islamic
AGO,Angola,Africa
GAB,Gabon,Africa
GIN,Guinea,Africa
LSO,Lesotho,Africa
MUS,Mauritius,unknown
unknown,Eswatini,
OMN,Oman,Islamic
MMR,Myanmar,South Asia
ETH,Ethiopia,Islamic
LBY,Libya,Africa
SUR,Suriname,Africa
unknown,Northern Cyprus,
BTN,Bhutan,South Asia
unknown,Nagorno Karabakh,
SOM,Somalia,Africa
SSD,South Sudan,Africa
GMB,Gambia,Africa
MDV,Maldives,South Asia
"""
    from io import StringIO
    import numpy as np
    df=pd.read_table(StringIO(csv),sep=',').replace('unknown',np.nan)

    # Double check I can drop Inglehart_groups_2008_retired_missing_ISO3:
    print('\n  Mismatches between my original 2008 list and the final hardcoded list:\n')
    for k,vlist in Inglehart_groups_2008_retired_missing_ISO3.items():
        for v in vlist:
            try:
                assert v in df.country.values
                assert k in df.Inglehart2008.values
                assert df.query(f'country=="{v}"').Inglehart2008.item() == k
            except (AssertionError,ValueError): #(' idiot check failed'):
                print(k,v, df.query(f'country=="{v}"').Inglehart2008.values)

    #Inglehart_groups_2008_retired_missing_ISO3 ={ # So this seems entirely redundant, given that I have a lookup including ISO3 below.


    
    return df

Inglehart_2008 = hardcoded_Inglehart_data()
print(' cpblUtilities:Inglehart cultural groups: we do not have assignments for the following:\n', Inglehart_2008[pd.isnull(Inglehart_2008.Inglehart2008)])
print('  cpblUtilities:Inglehart cultural groups: we do not have ISO3 for the following:\n', Inglehart_2008[pd.isnull(Inglehart_2008.ISO3)])
Inglehart_2008= Inglehart_2008.dropna(subset=['ISO3','Inglehart2008'])
Inglehart_2008['Inglehart2008']= pd.Categorical(Inglehart_2008.Inglehart2008)
Inglehart = Inglehart_2008


""" BTW, the hardcoded data above was generated using the older (partial)  assignment from WVS, plus my hand-coded additions, using the code below:

    from cpblUtilities.country_tools.Inglehart_groups import  Inglehart_groups_2008 as ig,Inglehart_GWP_not_in_WVS as igm
    if 'cultgroup' in df:
        print('DF already has "cultgroup" column!')
        print( df[['ISO3','country','cultgroup']].drop_duplicates().cultgroup.value_counts() )
    
    nc = sum([len(v) for v in ig.values()])
    rig = [[[country,region] for country in cl] for region,cl in ig.items()]
    rig = dict([item for sublist in rig for item in sublist])
    nrc = len(rig)

    missing = [c for c in df.country.unique() if c not in rig]
    print(missing)
    df['cultgroup'] = df.country.map(lambda ss: rig.get(ss,np.nan))
    print('After using WVS lookup, DF has:')
    # Create a lookup from our data, so we can avoid overwriting these ones from WVS, when we merge with my by-hand lookup below
    byISO3= df[['ISO3','country','cultgroup']].drop_duplicates().set_index('ISO3')
    print(byISO3.cultgroup.value_counts() )
    # Now fill in missing values:
    #df['cultgroup'] = df.apply(lambda adf: Inglehart_GWP_not_in_WVS.get(rig.get(ss,np.nan))
    # Following loses some rows with unknown ISO3:
    igm=igm.query('ISO3!="unknown"').set_index('ISO3').rename(columns={'Inglehart2008':'cultgroup'}).dropna(subset=['cultgroup'])[['cultgroup']]
    print('And igm comes with:\n', igm.cultgroup.value_counts() )
    print('igm is missing: \n', igm[pd.isnull(igm.cultgroup)])
    #fdf= igm.join(
    igm.update(byISO3)
    print('After first update::\n', igm.cultgroup.value_counts() )
    byISO3.update(igm)
    byISO3 = byISO3.dropna(subset=['cultgroup'])[['cultgroup']].reset_index().query('ISO3!="unknown"').set_index('ISO3')
    df=df.set_index('ISO3')
    df.update(byISO3)

"""
# Following stolen from bin/focal/common.py:

ingleOrder = dict([(b,a) for a,b in enumerate([
    'Africa',
    'Islamic',
    'Confucian',
    'South Asia',
    'Orthodox',
    'Protestant Europe',
    'English speaking',
    'Catholic Europe',
    'Latin America',
    ])])
ingleColours = {# from Cultural_map_WVS5_2008.jpg
    'Confucian': '#E8944C',
    'Catholic Europe': '#B7D034',
    'Protestant Europe': '#E6E626',
    'English speaking': '#CBBF1F',
    'Latin America': '#99B1B5',
    'Africa': '#B4AE40',
    'Islamic': '#BAB7BE',
    'South Asia': '#339D52',
    'Orthodox': '#D25840',
    }
# Overwrite with new set of colours?? Latin and Islamic need omre difference. Maybe just reorder them 
from cpblUtilities.color import unordered_discrete_colors
kk =  list( ingleColours.keys() )
ingleColours = dict( [ (kk[i], unordered_discrete_colors(len(ingleColours))[i]) for i in range(len(kk))])


ingleColours = dict([[cg, ingleColours[cg]] for cg in ingleOrder.keys()])

#ingleColours = dict([[cg, ingleColours[cg]] for cg in ingleOrder.keys()])


# Following copied from bin/focal/country_paper_common.py.  Note here if you use it, since it's rather over-specific
def add_ingle_colours_annotation(ax, loc='NE', fontsize=None, dy=None, nse=None):
    """
    xy given in axes-scaled (0,1) coordinates
    """
    # Can use mpl bounding boxes to align these better...
    dy=.05 if dy is None else dy
    otherArgs=dict(        transform = ax.transAxes, fontsize = fontsize,
                           bbox= dict(facecolor='white', alpha=.7, edgecolor='None'))
    #backgroundcolor='white',    )
    #def xxyy(iy,ingleOrder,loc):
    
    if loc =='NE':
        h = [
            ax.text(.97, .95-iy*dy, ingle, color = ingleColours[ingle],
                    ha='right',
                     **otherArgs)
            for iy,ingle in enumerate(ingleOrder.keys())
            ]
    elif loc =='NW':
        h = [
            ax.text(.03, .95-iy*dy, ingle, color = ingleColours[ingle],
                    ha='left',
                     **otherArgs)
            for iy,ingle in enumerate(ingleOrder.keys())
            ]
    elif loc =='SE':
        h = [
            ax.text(.97, .03+dy*(len(ingleOrder)-iy), ingle, color = ingleColours[ingle],
                    ha='right',
                     **otherArgs)
            for iy,ingle in enumerate(ingleOrder.keys())
            ]


    from cpblUtilities import bottomrighttext
    if nse:
        bottomrighttext(ax, 'ci: $\\pm${}$\\sigma$'.format(nse if isinstance(nse,int) else f'{nse:.1f}'), size=6, edgecolor='none')
    return h
