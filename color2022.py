#!/usr/bin/env python
import pandas as pd
import  numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import matplotlib as mpl

def addColorbarNonImage(data2color=None,data=None,datarange=None,cmap=None,useaxis=None,ylabel=None,colorbarfilename=None,location=None,ticks=None,
                        #preserve_axis_position=True,
                        cbax=None, # #optionally provide an inset_axes directly!
                        colorbar_ticks_side = None, **argin):
    """
    It adds a colorbar on the side to show a third dimension to a plot.
This 2022 version is intended to use the new "inset_axes" function

    "location" is not yet shown in docs, below, or in examples. It is passed straight to colobar.make_axes: [`None`|'left'|'right'|'top'|'bottom']

    Returns handle to colorbar object.  [But we need handle to axis]
     
    colorbarfilename: if given, we want to generate an SVG(?) file containing just the colorbar (probably to add to a geographic/svg map).

    ticks: a list of data values; sets the tick values for the colorbar

    argin: Some examples of things to use in argin, which are are passed on to ColorbarBase
         - aspect  (ratio of short to long dimension of colorbar) uh, no.. edit this.
         -  maybe: alpha=None, values=None, boundaries=None, orientation='vertical', ticklocation='auto', extend='neither', spacing='uniform', ticks=None, format=None, drawedges=False, filled=True, extendfrac=None, extendrect=False, label='')?  ) but the best way to choose its size is to pass the useaxis a pre-made/pre-sized axis, or to change the properties of the axis containing the object that is returned.
         Alternatively, you can find the axes by name (fig.axes()  --> [<AxesSubplot:>, <Axes:label='<colorbar>'>]) and adjust the size etc of the axis containing the colorbar afterwards.
       or maybe just
               cbax=plt.gca()
               pos1 = cbax.get_position() # get the original position 
               pos2 = [pos1.x0 + 0.3, pos1.y0 + 0.3,  pos1.width / 2.0, pos1.height / 2.0] 
               cbax.set_position(pos2) # set a new position

    Calling forms: (Always specify keywords explicitly! )
    
    Case 1: Use a matplotlib colormap, named and registered with plt.cm, and spread it out "linearly" over the given range of data.
        Here cmap can be: None (in which case 'jet' is used), a string name of a registered colormap like "hot", or cm.colormap object thingy (CHECK)

        addColorbarNonimage(data=data, cmap=cmap)
        addColorbarNonimage(datarange=[mind,maxd], cmap=cmap)

    Case 2: Supply a mapping from data values to RGB colours; build a custom mpl colormap for this relationship.
        This mapping d2c can be:  (a) a dict or (b) an interp1 function or (c) a pandas Series.
        Either way, this mapping from data values to RGB colours is probably generated with assignSegmentedColormapEvenly() or etc
    
        Case 2a: a dict is known

        addColorbarNonimage(d2cdict)
        addColorbarNonimage(data2color= d2cdict)
        addColorbarNonimage(datarange=[low,high],data2color= d2cdict) # This may not work yet. Restrict the dict to have just the desired range.

        Case 2a: an interp1 function is known

        addColorbarNonimage(data=data, data2color= d2c_interp1)
        addColorbarNonimage(datarange=[mind,maxd], data2color=d2c_interp1)

"""

    import matplotlib as mpl
    import scipy
    if 'ax' in argin: print(f"Warning: addColorbarNonImage: 'ax' is deprecated. Use 'useaxis' instead.")
    def check_if_numeric(a): # Tell a float or numpy float from  arrays, strings
       try:
           float(a)
       except (ValueError,TypeError) as e: 
           return False
       return True
    def get_data_range(data,datarange,data2color):
        if datarange is not None: return(datarange)
        if isinstance(data, (list, np.ndarray, pd.Series)):
            return(min(data),max(data))
        if isinstance(data2color, (list, np.ndarray,pd.Series)):
            return(min(data2color),max(data2color))
        if isinstance(data2color, dict):
            return(min(data2color.keys()),max(data2color.keys()))

    colorbar_ticks_side = 'right' if colorbar_ticks_side is None else colorbar_ticks_side
    if useaxis is None: useaxis=plt.gca()
    parent_axis_position = useaxis.get_position()
    parent_axis_xlim = useaxis.get_xlim()
    assert data is None or data.__class__ in [list, np.ndarray]
    assert datarange is None or isinstance(datarange,(list, np.ndarray))
    # We need information on the range of data:
    assert data is not None or ( isinstance(datarange,(list, np.ndarray))   and len(datarange)==2 ) or ( isinstance(data2color,(list, np.ndarray))   and len(data2color)==2 ) or isinstance(data2color,dict)
    # Specify either a color sequence (mapped from [0,1]) or a mapping from data values to RBG colours
    assert cmap is None or data2color is None 
    # We can specify cmap in two different ways, or induce the default value (with None)
    assert cmap is None or (isinstance(cmap,str) and cmap in plt.colormaps()) or isinstance(cmap,mpl.colors.LinearSegmentedColormap)
    assert data is None or datarange is None # Provide only the data or the range. For the moment, you cannot set a colorbar ylim that is different from the data range.

    mindata,maxdata=get_data_range(data,datarange,data2color)

    # Need to explicitly figure out calling form...
    
    knowncmap=  data2color is None

    if knowncmap: # cmap can be passed as a string or a cmap or an mpl.colors.LinearSegmentedColormap
        if isinstance(cmap,str): 
            cmapD=plt.cm.get_cmap(cmap)
        if cmap is None:
            cmapD = mpl.cm.jet
        elif cmap.__class__ in [mpl.colors.LinearSegmentedColormap]:
            cmapD=cmap 

    if not knowncmap:   # Prepare a cmap for if it's not specified. To do that, we need a dict mapping
        cmapName='_tmp'+str(mindata)+str(maxdata)
        if isinstance(data2color,dict):
            d2cDict=data2color
        elif isinstance(data2color,pd.Series):
            d2cDict=data2color.to_dict()
        elif isinstance(data2color,scipy.interpolate.interpolate.interp1d):
            if data is None: # Hard-code here to make a colormap using a linspace 256 long.  May have edge value problems?
                dx=maxdata-mindata
                data=np.arange(mindata,maxdata+dx*1.0/256,dx*1.0/256)
            d2cDict=dict([[xx,data2color(xx)] for xx in sorted(data)]) # series? ...
        else:
            raise('Cannot make d2c dict... Stuck')
        cmapD=  _colorAssignmentToColorbarmap(d2cDict,cmapname=cmapName )
        try:
            mpl.colormaps.register(cmap=cmapD)
        except ValueError:
            print('Warning: cmap already registered. Not registering again.')


    cmap=cmapD
        
    # Now, I believe the following is good, although maybe not if there are numerous duplicates in case when a lookup is passed (?)
    cnorm = mpl.colors.Normalize(vmin=mindata,vmax=maxdata)
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes
    if cbax is None:
        cbax = inset_axes(useaxis,
                   width="5%",  # width = 5% of parent_bbox width
                   height="100%",  # height : 50%
                   loc='lower left',
                   bbox_to_anchor=(1.00, 0., 1, 1),
                   bbox_transform=useaxis.transAxes,
                   borderpad=0,
                   )

    cb1 = mpl.colorbar.ColorbarBase(cbax, #mpl.colorbar.make_axes(useaxis,pad=0,location=location)[0],
                                    cmap=cmap,norm=cnorm,orientation='horizontal' if location in ['top','bottom'] else 'vertical', **argin)
    if ticks is not None: cb1.set_ticks(ticks)
    cbax=plt.gca()

    if colorbar_ticks_side != 'right':
        cbax.properties()['yaxis'].set_ticks_position( colorbar_ticks_side)
        cbax.properties()['yaxis'].set_label_position( colorbar_ticks_side)

    if ylabel is not None:
        cbax.set_ylabel(ylabel)

    # Thjis is in 2022, using inset_axes above, mostly likely the default behaviour, so is redundant
    
    # Now restore the original geometry of the parent axis, and its xlims, which matplotlib chose to screw up.
    if 0 and preserve_axis_position:
        # Assuming it's a vertical colorbar on the right (CAUTION!! it's not always), just restore x2:
        revised_parent_position = useaxis.get_position()
        dx = parent_axis_position.x1 - revised_parent_position.x1
        debug =False
        if debug: assert dx
        if debug: print(cbax.get_position())
        useaxis.set_position(parent_axis_position)
        if debug: print(cbax.get_position())
        plt.draw() # needed to change/update cbax position?!
        if debug: print(cbax.get_position())
        cbax_position = cbax.get_position()
        cbax_position.x1 = cbax_position.x1 + dx
        cbax_position.x0 = cbax_position.x0 + dx
        cbax.set_position(cbax_position)
        if debug: print(cbax.get_position())
        
    assert useaxis.get_xlim() == parent_axis_xlim  # I don't think we need to mess with this.
    plt.sca(cbax)
    return(cb1)#cbax)

def _colorAssignmentToColorbarmap(d2cDict,cmapname=None):
    """
    2013 Oct: I think I need yet another function. This one takes an assignment between data values and colors, in particular when they are not linearly interpolable from the data values, and generates a new colormap which, if scaled to the data range, would match the color assignment.

    It seems the only way to make a custom colormap is through the piecewise linear method, LinearSegmentedColormap.
    So can I just do this by taking every unique value of data and making a breakpoint at each?! ugh.

    WAIT! IS THIS ONLY USEFUL FOR PLOTTING COLORBARS? Is it used only by addcolorbarnonimage? If so, it should be inside there, no?
    WAIT! I need to use a cumsum of index, not the index of unique(): no? Do a harsher test of this than currently in demo...
    2014Dec: Without dealing with the above questions, I've rewritten this to make it more robust against repeated index values.

    2017: deal with dk=0 degenerate case (one value): what to do for lookup3?
    """
    def strictly_increasing(L):
        return all(y-x>1e-16 for x, y in zip(L, L[1:]))
    """
    def strongly_increasing(L):
        return all(y-x>.0001 for x, y in zip(L, L[1:]))
    def thinned_cdict(acdict):
        okaysteps= [[y-x>.0001 for x, y in zip([a for a,b,c in cdict[kk]], [a for a,b,c in cdict[kk]][1:])] for kk in cdict] 
        "
        allokay = 
        tokeep={}
        for kk in acdict:
            tokeep[kk]=[y-x>.0001 for x, y in zip(L, L[1:])]
        "
    """
    if cmapname is None: cmapname='tmpcm'

    # To make intent of this function plainer, deal first with the ideal /simple cases, leaving corners for below rather than vice versa:
    #if isinstance(d2cDict, dict) and len(d2cDict)>1 and strictly_increasing(d2cDict.keys():  [Not done!] 
    
    if len(d2cDict)==1: # Kludge horribly
        thekey=list(d2cDict.keys())[0]
        if thekey:
            d2cDict[thekey*1.00001] = d2cDict[thekey]
        else: # Sole value is zero!?! Kludge even more
            d2cDict[-1e-6] = d2cDict[thekey]
            d2cDict[+1e-6] = d2cDict[thekey]
            
    allkeys=list(d2cDict.keys()) 
    # Drop inifinite values? or explain here why they arise.
    assert all( np.isfinite(allkeys) )

    kmin=min(allkeys)
    kmax=max(allkeys)
    dk=kmax-kmin
    assert dk>0

    # Following 2 steps look like desperate kludges. Can someone make it nice?
    #
    # Put the values in order; and Scale the range to [0,1], using an integer index;
    lookup3=sorted([[int(1e6*(a-kmin)/dk), a]+list(b)  for a,b in list(d2cDict.items())])
    #
    # And use the integer index to get rid of duplicates, so as to ensure strictly increasing values:
    z2,ii=np.unique([L[0] for L in lookup3] , return_index=True)
    lookup=  [[a*1.0/1e6,b,c,d,e] for a,b,c,d,e in np.array(lookup3)[ii]]  # Recreate a dict with the subset of distinct values
    lookup[-1][0]=1.0
    assert strictly_increasing([a for a,b,c,d,e in lookup]) # By construction!
    
    ii,zz,rr,gg,bb=list(zip(*lookup))
    rgb=[rr,gg,bb]
    cdict=dict([ [cc,        np.array([ii,rgb[jj],rgb[jj]]).T]        for jj,cc in enumerate(['red','green','blue'])])

    newcmap = mpl.colors.LinearSegmentedColormap(cmapname, cdict)
    try:
        mpl.colormaps.register(cmap=newcmap)
    #except AttributeError: # after version 3.7? that doesn't exist anymore!
        
    except ValueError:
        print('Warning: cmap already registered. Not registering again.')
    
    #mpl.cm.register_cmap(cmapname, cdict)
    return(plt.get_cmap(cmapname))
    
