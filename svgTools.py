#!/usr/bin/env python
"""
Functions to convert a PostGIS table to svg, and colorize it

This would be easy to adapt to also handle shapefiles instead of the PostGIS input

On Ubuntu, this requires "sudo apt install nodejs optipng"  and "npm install svgexport -g"
and then adding a path like 
/home/cpbl/.npm/bin/svgexport 
to your $PATH!
"""
import os
import pandas as pd
#from osm_config import paths,defaults
try:
    import postgres_tools as psqlt
except ImportError as e:
    print(' Failed OSM imports ... this is not OSM repo! ')
    

class pg2SVG():
    """
    Creates a blank svg, and optionally colorizes it

    This approach uses mapshaper. An alternative is the PostGIS command, but this is far more complex.
    See http://gis.stackexchange.com/questions/134041/how-to-render-an-svg-from-postgis-using-st-assvg
    """
    def __init__(self,blankSvgFn,schema=None):
        assert blankSvgFn.endswith('.svg')
        self.blankSvgFn = blankSvgFn
        self.schema = schema
        self.outlineTable = None

    def createBlankSVG(self,table,idCol,geomCol='geom',groupby=None,srs=954009,where=None,outlines=False,simplify=False,dropSmall=False,clip=True,forceUpdate=False):
        """Creates and saves a blank SVG from a PostGIS table     
            
        where allows an optional WHERE clause to specify the rows that will be used
        groupby will aggregate the geometries by a list of fields
        simplify is False, or takes a tolerance parameter (in projected units). 
        simply=500 is not a bad option for projections in meters, e.g. Google Mercator (srs 3857) or Molllweide (954009)
        dropSmall drops islands under a certain area threshold. A good value is 10000**2 (10 sq km, assuming projection is in meters)
        idCol can be a single column, or a list which will be concatenated to form a single id
        """
        if os.path.exists(self.blankSvgFn) and not forceUpdate: return
        idList = idCol if isinstance(idCol,str) else ",'-',".join(idCol)
        if outlines and self.outlineTable is None:
            raise Exception('Must call self.prepareOutline() before asking for an outlined svg')


        cmdDict = {'geomCol':geomCol, 'srs':srs, 'where':where, 'table':table,'idList':idList,'tol':str(simplify)}

        if groupby is None: 
            cmdDict['geomCol'] = geomCol
        else:
            cmdDict['geomCol'] = 'ST_Union('+geomCol+')'

        #if clip:
        #    clipPoly = "ST_PolygonFromText('POLYGON((-169 -69,-169 75,179.9999 75,179.9999 -69,-169 -69))', 4326)"
        #    cmdDict['geomCol'] = 'ST_Intersection(ST_Transform(%s,4326),%s)' % (cmdDict['geomCol'], clipPoly) 

        self.db = psqlt.dbConnection(schema=self.schema)
        # Export to geoJSON - first part of command
        pgFlags = '"PG:dbname='+self.db.pgLogin['db']+' user='+self.db.pgLogin['user']
        if self.db.pgLogin['pw']!='': pgFlags+=' password='+self.db.pgLogin['pw']
        if 'host' in self.db.pgLogin: pgFlags+=' host='+self.db.pgLogin['host']
        
        if simplify is False:
            sqlCmd = 'SELECT ST_Transform(%(geomCol)s, %(srs)s) ' % cmdDict
        else:
            sqlCmd = 'SELECT ST_SimplifyPreserveTopology(ST_Transform(%(geomCol)s, %(srs)s), %(tol)s) ' % cmdDict
        sqlCmd +='''AS geom, concat(%(idList)s) AS id, ' polygon' as pathtype, 1 AS roworder FROM %(table)s %(where)s''' % cmdDict
        
        if groupby is not None:
            if isinstance(groupby, list):
                sqlCmd+=' GROUP BY %s' % (','.join(groupby))
            else:
                assert isinstance(groupby, str)
                sqlCmd+=' GROUP BY %s' % groupby
        
        if outlines:
            cmdDict = {'geomCol':self.outlineGeomCol, 'srs':srs, 'where':self.outlineWhere, 'table':self.outlineTable, 'tol':str(simplify)}
            #if clip:
            #    cmdDict['geomCol'] = 'ST_Intersection(ST_Transform(%s,4326),%s)' % (self.outlineGeomCol, clipPoly) 

            sqlCmd = 'SELECT geom, id, pathtype, roworder FROM ('+sqlCmd
            if simplify is False:
                sqlCmd += ') \nUNION ALL\nSELECT ST_Transform(%(geomCol)s, %(srs)s) ' % cmdDict
            else:
                sqlCmd += '\nUNION ALL\nSELECT ST_SimplifyPreserveTopology(ST_Transform(%(geomCol)s, %(srs)s), %(tol)s) ' % cmdDict
            sqlCmd +='''AS geom, '' AS id, 'outline' as pathtype, 2 AS roworder FROM %(table)s %(where)s''' % cmdDict
            sqlCmd +=') t1 ORDER BY roworder'  # guarantees that outlines will be ordered second, so rendered over

        cmd = 'ogr2ogr -f GeoJSON /vsistdout/ ' + pgFlags + '" -sql "'+sqlCmd+'"'
        # needs more recent GDAL
        #if clip:
        #    cmd+=' -spat -169 -69 180 75 -spat_srs 4326 -clipsrc spat_extent' # need more recent GDAL
            
        # Convert piped JSON to svg - second part of the command
        minAreaStr = '' if dropSmall==False else ' -filter-islands min-area='+str(dropSmall)
        cmd += ' | mapshaper -'+minAreaStr+''' id-field=id -svg-style "class=id+pathtype" -o '''+self.blankSvgFn+' force'
        print(cmd)
        os.system(cmd)
        
    def prepareOutline(self,table,geomCol='geom',where=None):
        """Allows for the SVG to have a separate set of paths, providing outlines (e.g. for country boundaries)
        Call this BEFORE calling blank svg
        This just sets up the properties. It doesn't actually do anything """
        self.outlineTable   = table
        self.outlineGeomCol = geomCol
        self.outlineWhere   = where

    def colorizeSVG(self,geo2data_or_color,outFn,data2color,cbylabel=None,addcolorbar=True,lw=0,outlineLw=1.0,pathStyle=None,colorbarlimits=None,fontsize=9,ticks=None,cbarpar=None,makePNG=False):
        """Based on colorize_svg in cpblUtilities.mapping, but adapted to the format of the blank svg above
        geo2data_or_color can be a pandas Series (index is the ids), or a dictionary of id to value mappings  
        cbarpar sets colorbar location
        """
        assert outFn.endswith('.svg')
        if isinstance(geo2data_or_color,dict): geo2data_or_color = pd.Series(geo2data_or_color)
        assert isinstance(geo2data_or_color, pd.Series)
        
        
        try:
            scratchpath=paths['scratch']
        except(ValueError,NameError) as e:
            scratchpath = './____tmp_'# if scratchpath is None else os.path.split(outfilename)[0]+'/____tmp_'

        from cpblUtilities.mapping import colors_for_filling_svg, addColorbar_to_svg
        hexlookup,d2c_cb=colors_for_filling_svg(geo2data_or_color=geo2data_or_color, data2color=data2color, colorbarlimits=colorbarlimits)

        import codecs        
        svgraw=codecs.open(self.blankSvgFn,'r','utf8').read()
        svgraw = svgraw.split('>')  # so we can insert the CSS

        if pathStyle is None: pathStyle=' stroke="black" stroke-opacity="0.5" stroke-miterlimit="4"'
        svgraw[1] = svgraw[1] + pathStyle

        css = '''\n<style id="style_css_sheet" type="text/css"> 
                 /*
                 * Below are Cascading Style Sheet (CSS) definitions in use in this file,
                 * which allow easily changing how countries are displayed.
                 * polygon provides overriding rules for polygons, and the fill is specified for each country
                 */
                 .polygon{fill:#e0e0e0; fill-rule:evenodd; stroke-width:%s; }
                 .outline{fill:transparent; stroke-linecap:round; stroke-linejoin:round; stroke:black; stroke-opacity:1; stroke-width:%s;}
                  ''' % (str(lw),str(outlineLw) )
                    
        css += '.'+'\n.'.join([id+'{ fill: '+color+';}' for id, color in list(hexlookup.to_dict().items())])
        css += '\n</style'  # omit closing tag because this will be added in the join
        
        # Insert CSS at start of document, after <svg>
        outsvg = '>'.join(svgraw[:2]+[css]+svgraw[2:])
        if addcolorbar:
            cbarparToUse = dict(expandx=1.2, movebartox=715,movebartoy=12,scalebar=1.1,fontsize=fontsize)
            if cbarpar is None: cbarpar = {}
            cbarparToUse.update(cbarpar)
            finalsvg=addColorbar_to_svg(outsvg,data2color=d2c_cb, scratchpath=scratchpath, colorbarlimits=colorbarlimits, colorbar_ylabel=cbylabel,ticks=ticks,frameon=False,
                             colorbar_location=cbarparToUse)  # This is not refined
        else:
            finalsvg=outsvg

        with open(outFn,'wb') as fout:  # Switched to binary here... to allow for the fact that finalsvg is a bytes object in python3. But should change encoding specified in XML ! not done yet.
            fout.write(finalsvg)
        print(('     Wrote '+outFn))
        
        if makePNG:
            pngFn = outFn.replace('.svg','.png')
            os.system('svgexport %s %s' % (outFn, pngFn))
            os.system('optipng %s' % pngFn)
            print(('Wrote %s' % pngFn))
        

def mergeSVGs(svgFns,outFn):
    """Merges svgs created in pg2SVG. (May not work with other SVGS!)
    This is useful if you have outlines in one table that you want to superimpose on another
    The headers are taken from svg1, and the CSS and paths are merged
    This is deprecated now that we can do this directly in pg2SVG().
    """
    
    assert all([os.path.exists(svgFn) for svgFn in svgFns])
    import codecs        
    
    svgs = [codecs.open(svgFn,'r','utf8').read() for svgFn in svgFns]
    svgs = [svg.split('>') for svg in svgs]
    cssRows    = [[(ii,row) for ii, row in enumerate(svg) if 'Cascading Style Sheet (CSS)' in row] for svg in svgs]
    if any([len(csr)==0 for csr in cssRows]): raise Exception('All SVG files must have a CSS section')
    footerRowIds   = [[ii for ii, row in enumerate(svg) if row.strip().startswith('</svg')] for svg in svgs]
    assert all([len(fr)==1 for fr in footerRowIds])    # should only be one close tag
    
    # take the header rows, then the css, then the bodies, then the footer
    header = svgs[0][:cssRows[0][0][0]]
    css = [row[1] for csr in cssRows for row in csr]
    css = [css[0]]+[row.strip('>') for row in css[1:]]  # get rid of intermediate style open tags
    css = '\n'.join([row.strip('</style') for row in css[:-1]]+[css[-1]])  # get rid of intermediate style close tags
    body = [row for csr,fid,svg in zip(cssRows,footerRowIds,svgs) for row in svg[csr[0][0]+1:fid[0]]]
    footer = svgs[0][footerRowIds[0][0]:] 
    finalsvg = '>'.join([row for rowList in [header,[css],body,footer] for row in rowList])


    with open(outFn,'w') as fout:
        fout.write(finalsvg)
    print(('     Wrote '+outFn))    
    
    
        
    
    
    
    
