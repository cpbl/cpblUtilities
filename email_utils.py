#!/usr/bin/env python
import os

# Following was used for Beijing robots!
def email_to_us(addressees_list, subj = ' (no subject)...' ,bodyfile=None, bodytext=None, attachment=None):
    assert bodyfile  is None or  bodytext is None
    if bodytext:
        with open('/tmp/.tmp_mbody.txt','w') as fout:
            fout.write(bodytext+'\n')
        bodyfile = '/tmp/.tmp_mbody.txt'
    subj=subj.replace('"','')
    if attachment is None:
        attachment = ''
    else:
        assert os.path.exists(attachment)
        assert ' ' not in attachment
        attachment = ' -a {} '.format(attachment)
    if isinstance(addressees,list):
        addressees = ','.join(addressees).replace(' ','')
    os.system('  mutt -s "'+subj+ '" '+addressees+ attachment + '  < '+bodyfile)

# Updated version of above. Used for production, eg. CG2024
# Oh no!! This failed after just a few messages: quota exceeded!
def email_using_mutt(addressees_list, subj = ' (no subject)...' ,bodyfile=None, bodytext=None, attachment=None,
                     cc=None, #bcc=None,
                     sleep =5,
                     test_only=False):
    """ There is no test mode. Just use a cpblpublic+ address for testing.
    """
    assert bodyfile  is None or  bodytext is None
    if bodytext:
        with open('/tmp/.tmp_mbody.txt','w') as fout:
            fout.write(bodytext+'\n')
        bodyfile = '/tmp/.tmp_mbody.txt'
    subj=subj.replace('"','')
    if attachment is None:
        attachment = ''
    else:
        assert os.path.exists(attachment)
        assert ' ' not in attachment
        attachment = ' -a {} '.format(attachment)
    if isinstance(addressees_list,list):
        addressees = ','.join(addressees).replace(' ','')
    else:
        addressees = addressees_list
        
    cc = f' -c {cc} ' if cc is not None else ''

    shcmd ='  mutt -s "'+subj+ '" '+addressees+ cc + attachment + '  < '+bodyfile
    if test_only:
        print(shcmd)
        print(f'sleep {sleep}')
    else:
        os.system(shcmd)
        print(f'Sent "{subj}" to {addressees}  unless you see an error from mutt ?! ')
        import time
        time.sleep(sleep)
        return True # errr.. but is it?

        

def email_using_sendmail_test_email_works_on_sprawl_2024():
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders

    # Email details
    sender_email = "chris.barrington-leigh@mcgill.ca"
    receiver_email = "cpblpublic+testing@gmail.com"
    subject = "Test Email with PDF Attachment"
    body = "This is a test email with a PDF attachment."
    pdf_filename = "drafts/supportLetter-for-VMorrison.pdf"

    # SMTP server configuration
    smtp_server = "localhost"
    smtp_port = 25  # Adjust if your server uses a different port
    #smtp_username = "your_email@example.com"  # If needed
    #smtp_password = "your_password"  # If needed

    # Create a multipart message
    message = MIMEMultipart()
    message['From'] = sender_email
    message['To'] = receiver_email
    message['Subject'] = subject

    # Attach the plain text body
    message.attach(MIMEText(body, 'plain'))

    # Attach the PDF file
    with open(pdf_filename, "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {pdf_filename}",
        )
        message.attach(part)

    # Connect to the local SMTP server and send email
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        # Uncomment the following lines if your SMTP server requires authentication
        # server.login(smtp_username, smtp_password)
        server.sendmail(sender_email, receiver_email, message.as_string())

    print("Email sent successfully!")
    return True # errr.. but is it?



















def add_attachment(msg, filepath): #https://stackoverflow.com/questions/61548551/cant-send-an-attachment-to-my-email-using-smtplib
    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(filepath, "rb").read())
    encoders.encode_base64(part)
    
    part.add_header('Content-Disposition', 'attachment', filename=attachment)
    
    msg.attach(part)



def NOT_USED_send_text_email_with_attachment(subject, body, to_email, from_email, smtp_server, smtp_port, login, password, pdf_path):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders

    # Create the email object
    msg = MIMEMultipart()
    msg['From'] = from_email
    msg['To'] = to_email
    msg['Subject'] = subject

    # Attach the plain text message body
    msg.attach(MIMEText(body, 'plain'))

    # Open the PDF file in binary mode
    with open(pdf_path, 'rb') as attachment:
        # Create a MIMEBase object
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())
        
        # Encode the payload using Base64
        encoders.encode_base64(part)
        
        # Add a header for the attachment
        part.add_header('Content-Disposition', f'attachment; filename="{pdf_path}"')
        
        # Attach the PDF file to the email
        msg.attach(part)

    # Set up the SMTP server and send the email
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()  # Secure the connection
        server.login(login, password)
        server.sendmail(from_email, to_email, msg.as_string())
    
