#!/usr/bin/env python3
"""
This assumes there's a local requirements.txt file made from pip freeze
"""
#from osm_config import paths, defaults,  update_paths_to_be_specific_to_database



import os
import time
def is_file_older_than_x_days(afile, days=1):
    if not os.path.exists(afile): return True
    file_time = os.path.getmtime(afile) 
    # Check against 24 hours
    #print( time.time(), file_time)
    return ((time.time() - file_time) / 3600 > 24*days)

def check_requirements(days=1, forceUpdate=False):
    #BP=paths['bin']
    RF, CF = 'requirements.txt',  'tmp-reqcheck.txt'
    #print(RF,CF)
    #print(is_file_older_than_x_days(CF,1))
    if forceUpdate or is_file_older_than_x_days(CF,days):
        os.system('pip3 freeze -r {} > {}'.format( RF,CF))
    import pandas
    dfc=pandas.read_csv(CF, sep='==', names=['package','version'], engine='python')
    dfr=pandas.read_csv(RF, sep='==', names=['package','version'], engine='python')

    df = dfc.join(dfr.add_suffix('_req'))
    df = df.dropna() # Drop all rows not in requirements.txt
    df2 = df[df.version != df.version_req]

    if df2.empty: return True
    if not forceUpdate: # File exists, but maybe it's out of date
        return check_requirements(days=days, forceUpdate=True)

    print(""" Some package versions do not agree:
    \n"""+ str(df2[['package','version','version_req']].rename(columns={'version':'Current version', 'version_req':'Version in '+RF}).set_index('package'))+"""
   """)


    print(' \n\nTo update everything that does not match, you would do:\n   pip3 install --user --upgrade {}\nand then \n   pip3 freeze -r {} > requirements-new.txt\n   and then merge them in'.format(' '.join(df2.package.values), RF))

    print(' \n\nTo update everything listed, you would do:\n   pip3 install --user --upgrade {}\n '.format(' '.join(df.package.values),))

    
    
    I=input('Please update the packages or change {}, or type "O" to override and continue:  '.format(RF))
    assert I.lower()=='o'
    
