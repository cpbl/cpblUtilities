cpbl-tables
===========

Conversion of tabular data to to modular LaTeX format for later on-the-fly layout choices/changes to table formatting.

Also, Extraction of tabular data from Stata, from Excel, from LibreOffice. 
