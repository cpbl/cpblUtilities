#!/usr/bin/env python
import pandas as pd
import numpy as np

def make_dummies_from_categorical(df, col, prefix, lookup=None, replace_dashes=True):
    """ Returns a tuple.
         Its first element is a copy of the data frame with new columns added, which are dummies for column `col`
         The second element is a list of the new column names

    col: the categorical column to use (or could it be numeric?)
    
    prefix: used to name the new dummy variables

    lookup: an optional dict mapping from string-ified values (of col) to a string value which gets appended to the prefix as a dummy column

    replace_dashes: only if lookup is generated 

    This uses Pandas' own get_dummies command! which names the columns with a string version of their value. That's why we need to map from string-ified values of col.
    """
    if lookup is None:
        lookup = dict([(a,str(a)) for a in df[col].unique()])
        if replace_dashes:
            columnmap = dict([(a,prefix+b.replace('-','_')) for a,b in lookup.items()])
    else:
        columnmap = dict([(a,prefix+b) for a,b in lookup.items()])

    dfd = pd.get_dummies(df[col]).rename(columns=columnmap)
    if dfd.columns.value_counts().max() >1:
        what_do_do
        # If multiple values are columnmap'd to the same dummy, we need to either do a logical_or afterwards, or replace them in groups.
        for k,v in columnmap.items():
            foiu
        
    dfd = dfd[[c for c in columnmap.values() if c in dfd.columns]]
    print('   Built dummies for column {}: {}'.format(col, dfd.columns))

    # Ensure we are not assigning dummies when our source column is null!

    if len(df) == len(dfd):
        dfd[df[col].isnull()] = np.nan
    else:
        assert len(dfd)==0        

    return  pd.concat([df, dfd], axis=1),   sorted( list(dfd.columns)  )
