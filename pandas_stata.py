#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import os
import numpy as np
import re
import time
"""
Some routines (used in bin/gallup and bin/focal) for loading stata datasets.  Built-in stata loader is not good enough in Pandas.  So we load everything twice -- once as its stored float value and once as its categorical value, in case there is one.
"""

from cpblUtilities import defaults, fileOlderThan
paths=defaults['paths']

# Not great. this is supposed to be defined only in recodeGallup_definitions.py.  It should be a required param, I guess
REFUSED_DK_VALS= ['(Refused)','(DK)','(RF)', 'Refused','Dont know','Not available', # CCHS??
                  "Don't know", 'Refusal', 'Not stated', 'No opinion', 'Valid skip', # GSS
                  '(Saw, skipped)', # GFS
                  ]

"""
This allows us to access df.svy.(features)
"""
@pd.api.extensions.register_dataframe_accessor("svy") # Survey mode accessor for DataFrame
class SurveyAccessor:
    def __init__(self, pandas_obj):
        self._validate(pandas_obj)
        self._obj = pandas_obj
        self._obj.value_labels=None  # N.B.! Pandas does not have a way yet to set an accessor property. So this is self._obj.value_labels rather than self.value_labels. ie. we just assigned a property to the dataframe object itself. But below, we can use the accessor formalism to provide methods to act on it.
        self._obj.variable_labels=None

    @staticmethod
    def _validate(obj):
        # verify there is dict of dicts which specifies value labels? nothing to validate yet...
        return
        if "vlabels" not in obj.columns or "longitude" not in obj.columns:
            raise AttributeError("Must have 'latitude' and 'longitude'.")

    @property 
    def value_labels(self):
        return self._obj.value_labels
        # return the geographic center point of this DataFrame
        lat = self._obj.latitude
        lon = self._obj.longitude
        return (float(lon.mean()), float(lat.mean()))

    def set_value_labels(self, value_labels):
        self._obj.value_labels = value_labels
        if not isinstance(value_labels,dict) or not all([isinstance(v,dict) for v in value_labels.values()]):
            raise ValueError("value_labels must be a dict of dicts.")

    def recode_missing_values(self, column=None, NaNvals=None, verbose=False):
        """ Look for "don't know", "refused", etc (strings given in NaNvals) as labels in value labels.  Recode the corresponding variable to NaN (missing) in these cases.
        """
        if NaNvals is None:
            NaNvals = REFUSED_DK_VALS
        woiuoiu
        if column is None:
            for column in self._obj.columns: #self._obj.value_labels.keys():
                self.recode_missing_values(column, NaNvals, verbose=verbose)
            return
        assert column in self._obj.columns
        
        if column in self._obj.value_labels:
            col_labels = self._obj.value_labels[column]
            if column.endswith('_LABEL') and column[:-6] in self._obj.columns and column[:-6] not in self._obj.value_labels:
                column = column[:-6]   
        elif column+'_LABEL' in self._obj.value_labels:
            col_labels = self._obj.value_labels[column+'_LABEL']
        else:
            print(f"Warning: cannot find a label for column {column}")
            return
        
        if not set(NaNvals).intersection( set( col_labels.values() ) ):
            # No data values are considered missing
            return

        NaNlabels  = {k:np.nan for k,v in col_labels.items() if v in NaNvals}
        assert NaNlabels
        if column not in self._obj.columns:
            print(f"Warning: column {column} not in dataframe. Pandas does not give us the mapping from labels to columns when they do not mat")
            wot
            return
        self._obj[column] = self._obj[column].replace(NaNlabels)

        if verbose:
            print(f"       Replaced {NaNlabels.keys()} with NaN in column {column} ")

                

    def replace_duplicate_vlabel(self, column=None, verbose=False):
        """
        
        """
        if column is None:
            for column in self._obj.value_labels.keys():
                self.replace_duplicate_vlabel(column)
            return

        col_labels = self._obj.value_labels[column]
        if len(col_labels) == len(set(col_labels.values())): # no duplicates
            return
        # Find the first duplicate
        print(f"Not written yet: here handle replacement of duplicate labels for column {column}:\n   {col_labels}")
        # Why does this not abort python execution at the raise??
    """
        for k,v in value_labels.items():
            if list(value_labels.values()).count(v) > 1:
                break

            if len(v) > 1:
                labelname = k
                break
        labelname = self._obj.name
    """

def load_stata_and_labels(fn,  fix_missing_labels=True, convert_missing_labels_to_NaN=True, verbose=True):
    """ Rather than trying to use pandas categoricals, this approach loads the value labels as Stata has them stored, and keeps this lookup separate.

    The advantage of this approach is that we can more easily deal with value labels with repeated labels, eg a variable for which only the extreme values have verbal cues (like SWL questions!), and more importantly, we can immediately recode all recognized missing values (based on their labels) to NaN.  The latter is accomplished with         dfi.svy.recode_missing_values()
    
    Return a tuple: dataframe enhanced with a .value_labels property, a .variable_labels property, and some methods in .svy.

    """
    dfi=pd.read_stata(fn, convert_categoricals=False)

    sr = pd.io.stata.StataReader(fn)
    dfi.svy.set_value_labels( sr.value_labels() )
    print(f"Loaded value labels for {len(dfi.svy.value_labels)} variables in {fn}.")
    # Weird outcome: columns are lower case here, but upper in Stata, and labels are all upper!!
    if all([v.islower() for v in dfi.columns]) and all([k.isupper() for k in dfi.svy.value_labels.keys()]):
        (print,input)[int(verbose)](f"Warning: all columns are lower case, but all value labels are upper case. Converting columns to upper case. Weird. Okay?")
        dfi.columns = [c.upper() for c in dfi.columns]

    if fix_missing_labels:
        dfi.svy.replace_duplicate_vlabel(verbose=verbose)
    if convert_missing_labels_to_NaN:
        dfi.svy.recode_missing_values(verbose=verbose)

    assert dfi.value_labels
    dfi2=dfi.copy() # Defrag the columns!
    dfi2.svy.set_value_labels( dfi.value_labels)
    return dfi2

    
    
# WHAT ABOUT pandas_load_stata ?!?!!? It's in pandas_utils. Okay, moving it here. So now there may be duplicate functionality: (2023):

def pandas_load_stata(fn,  bad_categoricals='fix', c_prefix=None, i_prefix = 'i_', ):#fix_missing_labels=True):
    """  Deal with two things : 
           - Pandas does not preserve original integers in its categoricals ! So we have to load twice if we want to know both.
           - Pandas fails sometimes, e.g. when labels are not unique.  So allow for excluding some columns which can't be loaded.

    Sometimes you want to work primarily with categoricals. Then set i_prefix= 'i_' or etc to rename the non-translated versions of columns
    Alternatively, set c_prefix = 'c_' or etc, and i_prefix = None

    2023-12: New trick: bad_categoricals = 'skip': keep cycling through, detecting them.
    2024-01: New trick: see https://github.com/pandas-dev/pandas/issues/13923   for filling in the missing labels??
    """
    print(' In 2024 I wrote a new load_stata_and_labels() function.  Use that instead. It automatically deals with finding NaN values, etc, and avoids having to load the file twice. Indeed, there is no need to duplicate columns!   If only Python allowed NaN integers, it would be even prettier' )
    
    c_prefix='' if c_prefix is None else c_prefix
    i_prefix='' if i_prefix is None else i_prefix
    assert c_prefix or i_prefix
    bad_categoricals= [] if bad_categoricals is None else bad_categoricals
    # Use following three lines for la
    print(' Reading {} without categoricals...'.format(fn))
    dfi=pd.read_stata(fn, convert_categoricals=False)
    if bad_categoricals in ['skip', 'fix']:
        bad_categoricals_found = []
        success=False
        while True: # break (below) exits
            try:
                df=pd.read_stata(fn, columns = [c for c in dfi.columns if c not in bad_categoricals_found])
                print('  No bad_categoricals found.')
                break
            except ValueError as e:
                print('  Bad categorical: {}'.format(e))
                newcol = str(e).split(' are not unique')[0].split(' ')[-1]
                print(newcol)
                if bad_categoricals == 'fix':
                    sr = pd.io.stata.StataReader(fn)
                    vl = sr.value_labels()
                    
                    foiuou
                bad_categoricals_found.append(newcol)
                print('  Trying again, now with bad_categoricals = {}'.format(bad_categoricals_found))
                #time.sleep(.11)
        bad_categoricals = bad_categoricals_found
    print(' Reading {} with categorical ...'.format(fn))
    df=pd.read_stata(fn, columns = [c for c in dfi.columns if c not in bad_categoricals])
    df = df.rename(columns=dict([(c, c_prefix + c) for c in dfi.columns])).join(dfi.rename(columns=dict([(c, i_prefix + c) for c in dfi.columns])))
    return df




def shellcommand(s, description=''):
    print(description+': \n    '+s)
    return os.system(s)


def fix_columns_case(adf, toupper=False, tolower=False):
    dfn_anyUpper = any([c for c in adf if c==c.upper()])
    dfn_anyLower = any([c for c in adf if c==c.lower()])
    print(' There are {} upper case and {} lower case columns in noncategorical.'.format('NO' if not dfn_anyUpper else "SOME", 'NO' if not dfn_anyLower else "SOME"))
    columns = adf.columns
    if tolower:
        lowercase_to_raw = dict([[c.lower(),c] 
                                     for c in columns])        
        adf=adf.rename(columns= dict([[c, c.lower()] 
                                     for c in columns]))
        return adf,lowercase_to_raw
    ohoh
    
    """
        dfn_anyUpper = any([c for c in df_noncategorical if c==c.upper()])
        dfn_anyLower = any([c for c in df_noncategorical if c==c.lower()])
        print(' There are {} upper case and {} lower case columns in noncategorical.'.format('NO' if not dfn_anyUpper else "SOME", 'NO' if not dfn_anyLower else "SOME"))
        if 
            df_noncategorical.columns = [c.lower() for c in df_noncategorical.columns]

        lowercase_to_raw = dict([[c.lower(),c] 
                                     for c in columns])        
    
"""

# was replace_labeled_values_with_NaN(df, labelcol, NaNlabels=None, verbose=True, drop=False, test=False):
def replace_categorical_values_with_NaN(df, labelcol, NaNlabels=None, verbose=True, drop=False, test=False):
    """ This uses series.cat.remove_categories to remove forbidden values from data (ie replace them with NaNs).
    """
    if NaNlabels is None:
        NaNlabels = REFUSED_DK_VALS

    if labelcol not in df.columns:
        print('  Warning: labelcol {} not in df.columns'.format(labelcol))
        return -1
    # Determine if column of pandas df is categorical:
    is_categorical = pd.api.types.is_categorical_dtype(df[labelcol])
    print(f"{labelcol}  is_categorical: {is_categorical}")
    if is_categorical:
        if set(NaNlabels).intersection( set( df[labelcol].cat.categories) ):
            df[labelcol]= df[labelcol].cat.remove_categories(NaNlabels)
            
    return df

def set_NaNs_in_float_column_based_on_values_in_categorical_column(df, labelcol, floatcol=None, NaNlabels=None, verbose=True, drop=False, test=False, float_prefix='float_'):
    """
    Examples:
      Case (1)
      set_NaNs_in_float_column_based_on_values_in_categorical_column(df, 'gender', 'float_gender', ['N/A'])
           Replaces float_gender with np.nan in rows where gender has categorical value 'N/A'

      Case (2)
      set_NaNs_in_float_column_based_on_values_in_categorical_column(df, 'float_gender', ['N/A'])
           Assuming 'gender' column also exists, should do same as above.

      Case (3)
      set_NaNs_in_float_column_based_on_values_in_categorical_column(df, 'gender', ['N/A'])
           Assuming 'float_gender' column also exists, should do same as above.

      Case (4)
      set_NaNs_in_float_column_based_on_values_in_categorical_column(df, 'gender', ['N/A'])
           Replaces gender with np.nan in rows where gender has CATEGORICAL OR STRING value 'N/A'


    drop=True  will also drop the rows with NaNs in the column to be edited.

    """
    if labelcol.startswith(float_prefix) and labelcol[(len(float_prefix)):] in df:  # (Case 2)
        float_col = labelcol
        labelcol = labelcol[len(float_prefix):]
        if test: print('case (2)')
    elif float_prefix+labelcol in df and floatcol is None:
        float_col = float_prefix+labelcol
        if test: print('case (3)')
    elif floatcol is None:
        float_col = labelcol
        if test: print('case (4)')
    else:
        float_col = floatcol
        
    if NaNlabels is None:
        NaNlabels = [
        'Missing; Unknown', 'Not asked', 'Not asked in survey', 'DonÂ´t know', 'DonÂ´t know', 'Don´t know', "Don't know", 'No answer',]
        noo

    if df[labelcol].dtype.name == 'category' or floatcol is None:
        vc=df[labelcol].value_counts()

        if any([v in vc.index for v in  NaNlabels]):
            mask = df[labelcol].isin(NaNlabels)
            vc2=df.loc[mask,labelcol].value_counts()
            #assert labelcol not in ['educationLevel','swl', 'a170']
            df.loc[mask,float_col]= np.nan  # So only goal is to set NaNs in the float_ version
            if verbose: print(' Removed refused/DK/etc values from {}:\n{}'.format(labelcol, vc2[vc2>0]))
    else:
        print(' Cannot handle {} in set_NaNs_in_float_column_based_on_values_in_categorical_column()'.format(labelcol))
        # Should really just proceed anyway, no?

    if drop:
        df.dropna(subset=[float_col], inplace=True)

def dtagz_to_temporary_dta(dtagz, forceRecode=False):
    rfn =        dtagz
    if rfn.endswith('.dta'): return rfn
    sname=  dtagz[:-7] if dtagz.lower().endswith('.dta.gz') else dtagz[:-6] if dtagz.lower().endswith('.dtagz') else dtagz
    pathname,sname = os.path.split(sname)
    tsfn =        paths['scratch']+sname+'.dta'  #os.path.split(dtagz)[1].replace('.dta.gz','.dta')
    if forceRecode or fileOlderThan(tsfn,rfn):
        print(f' Creating {tsfn} from {dtagz}...')
        shellcommand('gunzip -c {} > {}'.format(rfn, tsfn),     ' Ungzipping '+sname)
    return tsfn
    """ For what it's worth, here's my other code from pystata to do this:

    if fn.lower().endswith('.dta'):
        return fn
    pp, ff,ee = resolve_stata_filename(fn)
    assert ee in ['.dta.gz']
    sp = pp if use_scratch is False else paths['scratch']
    tempfile = sp + ff + '-tmp.dta' if tempfile is None else tempfile
    if fileOlderThan(tempfile, fn) or forceUpdate:
        print(('     {} > {}'.format(fn, tempfile)))
        os.system('gunzip -c {} > {}'.format(fn, tempfile))
    return tempfile
    """

def list_of_columns_in_dtagz(dtagz):
    """ Return a list of the column names, without invoking a full codebook or etc"""
    dtafile = dtagz_to_temporary_dta(dtagz, forceRecode=False)
    colfile = dtafile+'_columnlist.tsv'
    if not fileOlderThan(colfile,dtagz):
        return open(colfile,'rt').read().split('\n')
    df_noncategorical = pd.read_stata(dtafile, convert_categoricals=False)
    with open(colfile,'wt') as fout:
        fout.write('\n'.join(df_noncategorical.columns.to_list())+'\n')
    return list_of_columns_in_dtagz(dtagz)
    
def dtagz_to_double_columns(dtagz,
                            renameVarsList,
                            REFUSED_DK_VALS, INDEX_RAW_VAR_NAMES,
                            forceRecode=1,
                            stata_all_upper_case=False, # Mode for Gallup (not used for Gallup! Mode for GFS?)
                            gallup_kludge=False, # For stuff I don't understand but was working for gallup?
                            convert_all_to_lower_case=True, # Mode for WVS
                            value_label_problems=None, # This is a way to bypass problems? I'm  not sure it's needed with the twocolumn method??? 202202
                            dcfn=None, # specify name of saved file
                            merge_categorical_columns=None, # This is a kludge to handle the REGION?_* columns in GWP

                            codebook_for_renaming=None, # Optional: pass another dict whose keys we should rename like we do the data                            
                            ):
    """ 
    Adapted from recodeCCHS.py create_all_raw_slim_dataframe_versions
    The ugly part of this has to do with fiddlig with upper/lower case of columns.

    The stata file comes with a mixture of upper and lower case variable names.

    forceRecode values:
     10: do the last parts
     20: variable list has been updated; collect new variables
     30: redo the slow categoricals / non-categoricals load of the full dataset into Pandas
     90: start from the dta.gz 
     99: re-run the Stata codebook export ! and return the codebook; do not continue.
    -100: rsync sprawlc:projects/gallup/workingData/recoded-gallup2021.pandas ./  


    Comment: would it be nicer maybe to just use a makefile to control recoding processes, rather than this forceRecode number

    Currently, for Gallup World Poll, you should call this once with recode=99, so that you can look at the codebook and add all the REGION* variables to renameVarsList
    """

    value_label_problems = {} if value_label_problems is None else value_label_problems      # e.g. {'CCHS20152016-PUMF': ['DHHDGHSZ','PSC_015','PSC_075'],}
    merge_categorical_columns = {} if merge_categorical_columns is None else merge_categorical_columns  # Should be a dict
    if convert_all_to_lower_case:
        merge_categorical_columnsL = dict([(k.lower(), [vv.lower() for vv in v]) for k,v in merge_categorical_columns.items()])
    merged_cat_vars= list(merge_categorical_columns.keys())
    merged_cat_sources = sum(merge_categorical_columns.values(), [])
    
    
    dd=dict([a[:2] for a in renameVarsList])     # Does not yet have any conversion formulae..
    if dd:
        recodetable = pd.DataFrame({'newname':dd.keys(), 'gwp':dd.values()}).set_index('gwp')
        recodetable.index=recodetable.index.str.lower()
    else:
        recodetable=None # 

    
    rfn =        dtagz
    sname=  dtagz[:-7] if dtagz.lower().endswith('.dta.gz') else dtagz[:-6] if dtagz.lower().endswith('.dtagz') else dtagz
    pathname,sname = os.path.split(sname)
    
    rawnumdffn =        paths['scratch']+'rawdf-noncategorical-{}.pandas'.format(sname)
    rawdffn=    paths['scratch']+'rawdf-{}.pandas'.format(sname)
    dcfn =  dcfn if dcfn is not None else  paths['working']+'doublecolumned-{}.pandas'.format(sname)
    ####slimfn =    paths['working']+'recoded-slim-{}.pandas'.format(sname)
    statacodebookf=  f'{pathname}{sname}-statacodebook.pandas'.format(sname)

    if  forceRecode==0 and not fileOlderThan(dcfn,[rfn]):
        return dcfn,pd.read_pickle(dcfn)
   
    tsfn = dtagz_to_temporary_dta(dtagz)   #tsfn =        paths['scratch']+sname+'.dta'  #os.path.split(dtagz)[1].replace('.dta.gz','.dta')
    
    #if forceRecode and fileOlderThan(tsfn,rfn):
    #    shellcommand('gunzip -c {} > {}'.format(rfn, tsfn),     ' Ungzipping '+sname)

    if forceRecode>=99:# and fileOlderThan(statacodebookf,tsfn):
        from surveypandas import codebooks
        scb=codebooks.stataCodebookClass(fromDTA=rfn) # This uses dta.gz file
        if forceRecode==99:
            return scb
    
    if forceRecode>=30 or fileOlderThan(rawnumdffn,tsfn):
        #os.system(' rm {}'.format(rawnumdffn))  # That seems a bit extreme
        print('    [30] Checking for non-screwed up (by Stata) columns for '+sname+' ... Only run this stuff on the server!')
        df_noncategorical = pd.read_stata(tsfn, convert_categoricals=False)
        print(' Saving df_noncategorical as '+rawnumdffn)
        ### NO ! df_noncategorical.columns = df_noncategorical.columns.str.lower()
        df_noncategorical.to_pickle(rawnumdffn)
        
    if forceRecode>=20 or fileOlderThan(rawdffn, rawnumdffn):#  or (                os.path.exists(rawdffn) and any([c not in R for c in recodetable[sname].to_list() for R in  [pd.read_pickle(rawdffn).columns]])                ):
        print('   [20] (Re)creating '+rawdffn)
        problems = [] if sname not in value_label_problems else value_label_problems[sname]
        df_noncategorical = pd.read_pickle(rawnumdffn)
        df_noncategorical, lowercase_to_raw= fix_columns_case(df_noncategorical, tolower=convert_all_to_lower_case)


        

        #upcase = columns[0] == columns[0].upper() # Not safe for GWP; some are upper, some lower
        assert recodetable is not None and not recodetable.empty
        knowncolumns = [lowercase_to_raw.get(c,c) for c in np.unique(recodetable.index.to_list()+ INDEX_RAW_VAR_NAMES)] # Mixed case
        # We want to keep the noncategorical versions of problem variables, if they are known. We bring these back in again later.
        keepcolumns = [c for c in knowncolumns if c.lower() not in problems and c not in problems]

        missedcolumns = [c for c in knowncolumns if c.lower() in problems or c in problems]
        #keepcolumns =  keepcolumns if upcase else [c.lower() for c in keepcolumns]
        if gallup_kludge:
            if [c for c in keepcolumns if c.lower() not in columns]:
                print('  *** WARNING ***  Expecting columns that are not in the data. Problem in concordance table? *** ',[c for c in keepcolumns if c.lower() not in columns])

        if missedcolumns:
            print("  *** WARNING ***  The stata saving bug is giving us a problem importing the following: {}. We'll bring them in as non-categorical floats. *** ".format(missedcolumns))
        print(' {sname} has {lc} columns in raw file, {lknownc} known columns, {lbc} bad columns.\n   dta -> df for {sname} (keeping {lkeepc} columns) '.format(sname=sname, lc =len(df_noncategorical.columns), lknownc=len(knowncolumns), lbc = len(problems), lkeepc = len(keepcolumns)))

        rekeep=keepcolumns +  merged_cat_sources
        print(f' Re-loading stata into Pandas, converting categoricals, keeping only {rekeep}.')
        # We kept the mixed case in the rawnumdffn because we want to be able to call the line below with the right names that are in tsfn
        df = pd.read_stata(tsfn, columns= rekeep)




        if 0: 
            for stem in ['REGION', 'REGION2']:
                df[stem.lower()]=np.nan
                #df=df.copy()
                newcol= [np.nan]*len(df)
                for ISO in df.COUNTRY_ISO3:
                    regcol = stem+'_'+ISO
                    if regcol not in df: continue
                    mask = df.COUNTRY_ISO3 == ISO
                    newcol[mask]=df.loc[mask, regcol].astype(str)
                    #df.loc[mask, stem.lower()] = df.loc[mask, regcol].astype(str)
                    foooo
        
        #from pandas.api.types import union_categoricals
        #newcat=union_categoricals([df[c] for c in df.columns if c.startswith('REGION_')], ignore_order=True)
        #newcat2=union_categoricals([df[c] for c in df.columns if c.startswith('REGION2_')], ignore_order=True)

        newcolumns=[] # Keep track of any columns that are not in the df_noncategorical
        # Before we change case, create some merged columns:
        for km, vm in merge_categorical_columns.items():
            """
            start_time = time.time()
            df['col'] = df[vm].ffill(axis=1).iloc[:,-1]#.astype('category')
            end_time = time.time()
            print("it took this long to run: {}".format(end_time-start_time))
            start_time = time.time()
            # Let's try that again!
            print(f'    Doing maybe-faster? combining of categorical columns for "{km}". See https://stackoverflow.com/questions/74748734/combine-multiple-categorical-columns-into-one-when-each-row-has-only-one-non-na/74748772 ' , time.time()            )                       def combine_first_valid_categorical(x):
                m = ~ pd.isnull(x)
                if m.any():
                    return x[m]
                else:
                    return np.nan
                
            df[km] = np.apply_along_axis(combine_first_valid_categorical, 1, df[vm].to_numpy()) # Magically creates new categorical!
            print(f'       ... New column {km} created.', time.time()  )
            end_time = time.time()
            print("it took this long to run: {}".format(end_time-start_time))
            """
            start_time = time.time()            
            print(f'    Doing ~100s-long combining of categorical columns for "{km}". See https://stackoverflow.com/questions/74748734/combine-multiple-categorical-columns-into-one-when-each-row-has-only-one-non-na/74748772 ', start_time)
            df[km] = df[vm].dropna(how='all').apply(lambda x: x.loc[x.first_valid_index()], axis=1).astype('category') # From my StackExchange question! N.B. This relies on index-matching, since the RHS is shorter than df due to some rows being all NaN.
            vmf = ['float_'+c for c in vm]
            df=df[[c for c in df if not c in vm+vmf]].copy()
            end_time = time.time()
            print(f'       ... New column {km} created.', end_time)
            newcolumns+=[km]
        
        if missedcolumns: # Shouldn't this be renamed to float_ ?
            df = df.join(df_noncategorical[[c.lower() for c in missedcolumns if c.lower() in df_noncategorical]])

        ccolumns = df.columns
        if convert_all_to_lower_case:
            df.columns = [c.lower() for c in df.columns]

        # This below is presumably identical to the other one! I'm just thrashing around now... to be all cleaned up using tests.
        # "c" prefix means categorical
        clowercase_to_raw = dict([[c.lower(),c] 
                                     for c in ccolumns])        
        
        if 0:#not upcase:
            df_noncategorical.columns= [c.upper() for c in df_noncategorical.columns]
            df.columns= [c.upper() for c in df.columns]
        assert len(df)==len(df_noncategorical)
        #if missedcolumns: # Move to earlier
        #    df = df.join(df_noncategorical[missedcolumns])
        #
        print('   Joining non-categorical with categorical versions to produce '+rawdffn)
        df_m = df.join(    df_noncategorical[[c for c in df.columns if c not in newcolumns]].add_prefix('float_')  )  # Why all columns instead of just needed non-cat version?
        df_m.columns = df_m.columns.str.lower() # Now we are done forever with the mixed-case silliness.
        df_m.to_pickle(rawdffn)
        

    # Continue from here even if forceReload is false; unless forceRecode is also set to be false
    if forceRecode<10 and not fileOlderThan(dcfn, rawdffn):
        print(f'   [10] Loading {dcfn}')
        return dcfn,pd.read_pickle(dcfn)

    
    # Now rename and rescale the float versions
    df = pd.read_pickle(rawdffn)




    
    # Remove all easily-identified integer nan-codes from float_* values:
    #assert 'wp3117' in df.columns

    for col in [cc for cc in df.columns if not cc.startswith('float_')]:
        set_NaNs_in_float_column_based_on_values_in_categorical_column(df, col, 'float_'+col, NaNlabels= REFUSED_DK_VALS, verbose=True)
        """
        if df[col].dtype.name == 'category':
            vc=df[col].value_counts()
            if any([v in vc.index for v in  REFUSED_DK_VALS]):
                mask = df[col].isin(REFUSED_DK_VALS)
                vc2=df.loc[mask,col].value_counts()
                #assert col not in ['educationLevel','swl', 'a170']
                df.loc[mask,'float_'+col]= np.nan  # So only goal is to set NaNs in the float_ version
                print(' Removed refused/DK/etc values from {}.\n{}'.format(col, vc2[vc2>0]))
        """

    # Rename known variables to favoured names:
    print(' Renaming ...   (interestingly, not instantaneous!)')
    df = df.rename(columns = recodetable.to_dict()['newname'])

    recodetablef = recodetable.copy()
    recodetablef['floatname'] = 'float_'+ recodetablef.newname
    recodetablef.index = 'float_'+ recodetablef.index
    df = df.rename(columns = recodetablef.to_dict()['floatname'])
    df['constant']=1


    
    if codebook_for_renaming is not None:
        cb = dict([(k.lower(),v) if convert_all_to_lower_case else (k,v) for k,v in codebook_for_renaming.items()])
        rtd = recodetable.to_dict()
        cbr = dict([(v, cb.get(k,{})) for k,v in recodetable.newname.to_dict().items() if k in cb])

        codebook_for_renaming.update(cbr)
        
        #recodetable.to_dict().items(): # modify this dict to also include the renamed versions:
        """
    for fr,to in (codebook_for_renaming is not None) * 
        if fr in codebook_for_renaming or (convert_all_to_lowercase and 
        .update(dict([]))
codebook_for_renaming.update        
        for k,v in codebook_for_renaming.update.items()]
"""
    
    """
    import matplotlib.pyplot as plt
    df.float_lifeToday.hist()
    plt.show()

    """
    df.to_pickle(dcfn)
    ##print('  Wrote {}.  Use that file in external projects.'.format(dcfn))
    return dcfn,df


################################################################################################
################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################


    # For testing, can use one of these:

    DTAGZ= '/home/meuser/projects/gallup/inputData/wvs/wave7/WVS_Cross-National_Wave_7_stata_v2_0.dta.gz'
    #from recodeGallup_definitions import OLD_FORMAT_renameVarsGallup, REFUSED_DK_VALS, INDEX_RAW_VAR_NAMES

    from pandas_stata import dtagz_to_double_columns
    dcfn,df = dtagz_to_double_columns(DTAGZ,
                                [('weight','w_weight'),
                                 ('year','a_year',),
                                 ('wave', 'a_wave'),
                                 ('ISO3', 'c_cow_alpha'),
                                 ('ISO3w', 'b_country_alpha'),
                                 ('SWL','q49'),
                                 ('satisFinances', 'q50'),
                                 ('education','q275'),
                                 ],#renameVarsList,
                                [],[], #REFUSED_DK_VALS, INDEX_RAW_VAR_NAMES                            
                                      forceRecode=90)
