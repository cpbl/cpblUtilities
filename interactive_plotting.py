#!/usr/bin/env python3

import numpy as np
import sys
import os
import pickle
import matplotlib  # workaround for bug in pympress https://github.com/Cimbali/pympress/issues/159
matplotlib.rcParams['pdf.fonttype']=42     
import matplotlib.pyplot as plt
import pandas as pd
from copy import deepcopy


class interactive_plotscreen_baseclass(object):
    """ Try to abstract some of the interactivity here, and then the actual plotting, etc functions can be updated for specific applications. In 2023, the only implementation of this is in bin/focal/plot_synthetic_analysis.py
    """
    def __init__(self, 
                 datadict,
                 slidervars, # This is a dict of variables and their starting values
                 xvar, # This is a dict of the x var and its starting value, or just a str of the x var
                 start=True, interactive=True, # start is only used when interactive is True.
                 verbose=False,
                 figsize=None, #Inches: (width, height)
                 layout=None,  # Different layouts of axes are possible. This is a string, usually 'default'
                 n_se = 1.96 #  Default confidence interval
                 ):
        self.verbose=verbose
        self.data = deepcopy(datadict)
        self.n_se = n_se
        
        #self.bias= self.data['estimates']#[-(self.data['bias'].parameter.str.contains('_SWL'))]

        #self.use_best_N = use_best_N # Not yet written: mode to use whatever data are available
        #self.hide_OLS = HIDE_OLS if hide_OLS is None else hide_OLS
        self.figsize = figsize if figsize is not None else (12,8.5+2)
        ##self.dgp= self.data['dgpbias'].query('variant=="DGP"')    # Not used?? This is currently empty. Should be col DGPb?

        ## Choose values for the in-principle reporting bias in mean SWL from focal value behaviour
        #theoretical_bias_in_meanSWL = (self.data['meanSWL'].DGP_mixture- self.data['meanSWL'].highAnalogue_DGP_mixture).dropna().reset_index().sort_values('N').groupby('hashname').last().rename(columns={0:'theoretical_bias'})
        ## Check that we are not relying on low-N samples:
        #Ns=theoretical_bias_in_meanSWL['N'].value_counts()
        #print(Ns)
        ## Overwrite unreliable values with NaN:
        #theoretical_bias_in_meanSWL.loc[theoretical_bias_in_meanSWL.N<2000,'theoretical_bias'] = np.NAN*theoretical_bias_in_meanSWL.loc[theoretical_bias_in_meanSWL.N<2000,'theoretical_bias'] 
        #self.meanSWLbias =  theoretical_bias_in_meanSWL['theoretical_bias']


        ## Use only the highest-N available for DGP distributions (and choose index)
        ## But for Stan posterior, and Stata posterior? we should keep multiple N, to properly represent sample sizes. And also to properly represent missing data when it is missing?  Or I could have an option (self.use_best_N, not yet fully implemented) to always show the best N rather than select among them.
        #self.data['dgp_distribution'] = self.data['dgp_distribution'].sort_values('N').groupby(['hashname','SWL']).last()
        #print( self.data['stan_posteriors'].N.value_counts() )

        #if self.use_best_N:
        #    self.data['stata_posteriors'] = self.data['stata_posteriors'].sort_values('N').groupby(['hashname','SWL']).last()
        #else:
        #    self.data['stata_posteriors'] = self.data['stata_posteriors'].sort_values(['hashname','N','SWL']).set_index(['hashname','SWL'])
        #if self.verbose:
        #    print('Stata posterior source:\n', self.data['stata_posteriors'].N.value_counts() )

        #if self.use_best_N:
        #    self.data['stan_posteriors'] = self.data['stan_posteriors'].sort_values('N').groupby(['hashname','SWL','subset']).last()
        #else:
        #    self.data['stan_posteriors'] = self.data['stan_posteriors'].sort_values(['hashname','N','SWL','stanvarname']).set_index(['hashname','SWL'])
        #if self.verbose:
        #    print('Stan posterior source:\n', self.data['stan_posteriors'].N.value_counts() )


        ## Store the DGP and Stan-mixture-estimated versions of the FVRI.
        #dgpfhigh= self.data['dgp_distribution'].reset_index().set_index(['SWL','hashname']).loc[-99]
        #fvri_dgp = dgpfhigh.assign(plow_DGP = 1-dgpfhigh.DGP_high)[['plow_DGP']]

        ## So the following may have more than one version of each (ie for different N) if not self.use_best_N
        #self.fvri =  self.data['stan_posteriors'].reset_index().set_index(['SWL','hashname']).loc[-96].join(  fvri_dgp )[['N','stanmixture','stanmixture_seL','stanmixture_seU','plow_DGP','stanmixture_mixhigh_param']]
        #
        #self.show_kludge_estimates =                  show_kludge_estimates 
        #self.ols_legend_done = False        
        #""" You can specify starting values by passing a dict for slidervals.
        #"""
        ##default_starting_values=dict( neg_cut_offset=7,  cut_scale=2,  mixhigh_param=1,  c_Ih=.45,  n_education4=4,        N=5000,)
        #default_starting_values=dict( neg_cut_offset=4.5,  cut_scale=2,  mixhigh_param=.65,  c_Ih=.45,  n_education4=4,        N=self.bias.query("variant=='stanmixture'").N.max())

        starting_values = {}
        if isinstance(slidervars , dict):
            starting_values.update(slidervars)
        if isinstance(xvar , dict):
            starting_values.update(xvar)
            assert len(xvar)==1
            xvar = list(xvar)[0]
        # Now xvar is a str.
        if not isinstance(slidervars , dict):
            slidervars = dict([(k,starting_values.get(k,None)) for k in slidervars])
        slidervars = [k for k in slidervars.keys() if not k==xvar]
        # Now slidervars is a list, and the starting values are in starting_values
        print(f'Starting values: {starting_values}; slider values: {slidervars}')
        self.starting_values = starting_values 
        
        slidersn = slidervars +[xvar]
        # The following two should be available for prep_data()
        self.xvar=xvar
        self.slidervars = slidervars
        #self.longnames = dict(neg_cut_offset = 'Cutpoint offset', mixhigh_param='N parameter', n_education4= r'$\beta^h_N$', N='Sample size',
        #                      cut_scale = 'Cutpoint scale', c_Ih=r'$\chi$')
        #

        # Following don't do anything by default, but useful if you want a toggle for something. Controlled by "d" key
        self.display_modes = ['mode 1', 'mode 2', 'mode 3']
        self.display_mode = self.display_modes[0]
        self.axes_layout = layout if layout is not None else 'default'
        
        self.prep_data()
        
        #allsv = self.bias[slidervars+[xvar]].drop_duplicates()
        self.discrete_vals = self.choose_discrete_slider_options(slidersn)
        #self.discrete_vals = dict([(k, np.array(sorted(allsv[k].dropna().unique()))) for k in slidersn])
        # Also invert the lists to make a lookup from value to index:
        self.discrete_val_index = dict([  (k,
                                           dict([(v,i) for i,v in enumerate(self.discrete_vals[k])])
                                           ) for k in slidersn])

        # Until I ditch the "starting values" above, kludge here to use them to update the 
        # Create sliders
        self.slider_index= dict([(k, 0) for k in self.discrete_vals.keys()])
        self.active_index = 0 #self.discrete_vals.keys()[0]
        self.set_slider_index(dict([(k,v) for k,v in starting_values.items() if k in slidersn]))
        self.caption_object= None

        
        assert self.slider_vals_available() # If this function has been redefined, this checks it, and checks that starting vals exist/ are available.
        self.interactive=interactive

        #from cpblUtilities.color import getIndexedColormap
        #nrows=5
        ##self.fig,self.axs = plt.subplots(5+int(interactive),2, figsize=(12,8.5+10*int(interactive)))
        #self.fig,self.axs = plt.subplots(5, 2, figsize=figsize)#*int(interactive)))  # width, height
        #HOA= int(self.hide_OLS)
        #self.axbyname = dict(
        #    dist=self.axs[0+HOA][0],
        #    meanSWL=self.axs[0+HOA][1],
        #    olo_bi =self.axs[1+HOA][1],  # Show bias
        #    olo_be =self.axs[1+HOA][0],  # Show bias
        #    ols_bi =self.axs[2-2*HOA][1],  # OLS coefficients
        #    ols_be =self.axs[2-2*HOA][0],  # OLS coefficients
        #    stan_be=self.axs[3][0],  # Show bias
        #    stan_bi=self.axs[3][1],  # Show bias
        #    #plot_predicted_distribution(Plows, Phighs, self.axs[0][0])
        #    fvri = self.axs[4][0],
        #    captions = self.axs[4][1],
        #    )
        #
        #plt.subplots_adjust(left=None, bottom=None if interactive else .075, right=None, top= None if interactive else 1, wspace=None if interactive else .045, hspace=.35 if interactive else .25)
        ##for i in range(nrows-2,nrows):  # LAYOUT to revise with cusotm todo
        ##for j in [0,1]:
        ##self.axs[i][j].set_visible(False)
        #for x in self.axs.flatten(): x.set_visible(False)
        #for k,x in self.axbyname.items():
        #    if not self.hide_OLS or k not in ['ols_be','ols_bi']: x.set_visible(True)
        #self.axbyname['captions'].axis('off')
        #for i in range(4):
        #    self.axs[i,1].yaxis.set_label_position("right")
        #    self.axs[i,1].yaxis.tick_right()
        #titles = dict(stan_be ='Education effect (bias, mixture)',
        #              stan_bi ='Income effect (bias, mixture)',
        #              olo_be ='Education effect (bias, ologit)',
        #              olo_bi = 'Income effect (bias, ologit)',
        #              ols_be ='Education effect (OLS,scaled)',
        #              ols_bi = 'Income effect (OLS, scaled)',
        #              meanSWL = 'Mean SWL, bias',
        #              fvri = 'Focal Index (FVRI)', # Fraction high type  : Show Stan's posterior and DGP value on same axes
        #              )
        #for k,t in titles.items():
        #    if 'bias' in t:
        #        self.axbyname[k].set_ylabel('Bias')
        #self.axbyname['ols_be'].set_ylabel('(scaled) OLS coefficient')
        #self.axbyname['fvri'].set_ylabel(titles['fvri'])

        #self.fhigh_color = 'brown'

        ##self.axbyname['pHigh'].yaxis.set_label_position('right')
        #for axn in  ['meanSWL',  'stan_bi', 'ols_bi', 'olo_bi',]:
        #    toprighttext( self.axbyname[axn], titles[axn].replace('bias, ',''))
        #for axn in  ['stan_be',   'olo_be', 'ols_be',]:# 'fvri',]:
        #    toplefttext( self.axbyname[axn], titles[axn].replace('bias, ',''))
        #self.axbyname['fvri'].set_xlabel(self.longnames.get(self.xvar,self.xvar))
        #self.axbyname['stan_bi'].set_xlabel(self.longnames.get(self.xvar,self.xvar))

        ##Share some axes: use the Stata ologit estimates, which get plot first / tight?! to make the Stan coef ones big enough:
        #self.axbyname['olo_bi'].get_shared_y_axes().join(self.axbyname['olo_bi'],      self.axbyname['stan_bi'])
        #self.axbyname['olo_be'].get_shared_y_axes().join(self.axbyname['olo_be'],      self.axbyname['stan_be'])
        ## All axes except for the distribution should share x:
        #for k in self.axbyname.keys():
        #    if not k in ['captions','dist','ols_be']:
        #        self.axbyname[k].get_shared_x_axes().join(self.axbyname[k],      self.axbyname['ols_be'])

        #
        self.to_del=[]
        self.to_gray=[]



        ## It seems we only need to draw these dashed black lines once.
        #xl = self.axbyname['olo_bi'].get_xlim()
        #for ax in [self.axbyname[a] for a in ['meanSWL', 'stan_be', 'stan_bi', 'olo_bi', 'olo_be',  ]]:
        #    #ax.set_xlim(xl)
        #    ax.plot(xl, [0,0], 'k--')
        #    #ax.set_xlim(xl)
        #for ax in [self.axbyname[a] for a in ['ols_bi', 'ols_be',]]:
        #    #ax.set_xlim(xl)
        #    ax.plot(xl, [1,1], 'k--')
        #    #ax.set_xlim(xl)
        #self.axbyname['olo_bi'].set_xlim(xl)  # All others are linked.
        #    
        ##In stead of legends, put some coloured text in between some axes:
        #x,y,ha,va = (1.01,.2,'left','bottom') if self.interactive else (1,.01,'right','bottom')
        #if self.interactive: # In fact, since it makes no sense to show the highhash trace in production, we don't need this legend:
        #    self._stack_matplotlib_text(self.axbyname['ols_be'],  x,y,
        #    [r'FVRI$\rightarrow$0','Naive OLS',]+self.show_kludge_estimates *['No focals', 'Collapsed'],
        #    ['k','m']+self.show_kludge_estimates *['c','y'],
        #                            horizontalalignment=ha,
        #                            verticalalignment=va,
        #    transform=self.axbyname['ols_be'].transAxes, mode='stacked')
        #x,y,ha,va = (1.01,.3,'left','bottom') if self.interactive else (1,.01,'right','bottom')
        #self._stack_matplotlib_text(self.axbyname['stan_be'],x,y,
        #                            [r'mixture', r'FVRI$\rightarrow$0'],
        #                            ['g','b'],
        #                            horizontalalignment=ha,
        #                            verticalalignment=va,
        #    transform=self.axbyname['stan_be'].transAxes, mode='stacked')
        #
        #x,y,ha,va = (1.01,.3,'left','bottom') if self.interactive else (1,.01,'right','bottom')
        #self._stack_matplotlib_text(self.axbyname['fvri'],x,y,
        #                            [r'mixture', 'DGP'],
        #                            ['g',self.fhigh_color],
        #                            horizontalalignment=ha,
        #                            verticalalignment=va,
        #    transform=self.axbyname['fvri'].transAxes, mode='stacked')

        #if not 'not debug':
        #    plt.savefig('tmp_startup.png')
        #    plt.savefig('tmp_startup.pdf')
        if start:
            self.set_up_axes() 
            #self.static_plot()
            self.start()


    def prep_data(self):
        # You should override this method in your child class!
        return

    def choose_discrete_slider_options(self, slidervars):
        # Now assume for the moment that sliders are all discrete (!). TO DO: generalize this.
        # We will look for them in the data, somehow.

        # Just make up some nonsense, but including the startin values, to demonstrate the format:
        return dict([(k, sorted([-1, 0, self.starting_values.get(k) ])) for k in slidervars])

        
    def start(self):
        self.mpl_cid = self.fig.canvas.mpl_connect('key_press_event', self.process_keypress)
        self.display_control_options()
        self.interactive = True 
        return

    def set_up_axes(self):
        noway #Delete this line. I'm debugging to make sure the child is called, not this...
        self.fig,self.axs = plt.subplots(1+int(self.interactive),2, figsize=(12,8.5+10*int(self.interactive)))

    def reset(self):
        return

    def cycle_display_mode(self):         # If needed, you can use this to toggle some display option
        # Advance display mode in list:
        self.display_mode = self.display_modes[  (self.display_modes.index(self.display_mode)+1)%len(self.display_modes)  ]

        return
    
    def display_control_options(self):
        active_control = list(self.discrete_vals.keys())[self.active_index]
        print('\n\n'+'-'*65+'\nControls:')
        for k,L in self.discrete_vals.items():
            print('{}\t{:20s}:\t{}'.format('<  >' if k==active_control else '', k,  '   '.join([' {} '.format(vv) if not ii==self.slider_index[k] else '*{}*'.format(vv)   for ii,vv in enumerate(L)])))
        print(' -'+'-'*30)
        print('\tup/down: select controls from above list')
        print('\tR: (R)eset axes')
        print('\td: change (D)isplay mode')
        print('\tE: (E)xport to PDF and display')
        print('\tQ: (Q)uit')
        print('-'*25+ ' Press keys inside the figure ' +'-'*25)
        
    def process_keypress(self,event):
        """ Options are: change "slider", move up, move down, quit, reset.

        The update0d and update1d are probably too specific for this base class!
        """
        sys.stdout.flush()
        active_control = list(self.discrete_vals.keys())[self.active_index]
        if event.key.lower() in ['q','x']:
            self.fig.canvas.mpl_disconnect(self.mpl_cid)
        if event.key.lower() in ['e','q','x']:
            self.fig.savefig('tmp-interactive_plotting.pdf')
            os.system('pdfcrop {f}  {f} && evince {f}&'.format(f='tmp-interactive_plotting.pdf'))
            plt.close('all')
        elif event.key.lower() in ['d']:
            # Do not make this a slider; too complicated. Just handle it another way.
            self.cycle_display_mode()
            ## self.slider_index[active_control] = (self.slider_index[active_control]-1 )  % len(self.discrete_vals[active_control])
            #self.display_control_options()                        
        elif event.key.lower() in ['r']:            
            self.reset()
            self.update0d()
            self.update1d()
        elif event.key.lower() in ['up','i','p']:
            self.active_index = (self.active_index -1 ) % len(self.discrete_vals)
            self.display_control_options()
            self.draw_caption()
        elif event.key.lower() in ['down','m','n']:
            self.active_index = (self.active_index +1 ) % len(self.discrete_vals)
            self.display_control_options()
            self.draw_caption()
        elif event.key.lower() in ['[','left','<','-','j']:
            while 1:
                self.slider_index[active_control] = (self.slider_index[active_control]-1 )  % len(self.discrete_vals[active_control])
                if self.slider_vals_available():
                    break
                else:
                    print(f" Value {self.discrete_vals[active_control][ self.slider_index[active_control]]} not available for {active_control}")
            self.update0d()
            self.update1d()
            self.display_control_options()
        elif event.key.lower() in [']','right','>','+','=','k']:
            while 1:
                self.slider_index[active_control] = (self.slider_index[active_control]+1 )  % len(self.discrete_vals[active_control])
                if self.slider_vals_available():
                    break
                else:
                    print(f" Value {self.discrete_vals[active_control][ self.slider_index[active_control]]} not available for {active_control}")
            self.update0d()
            self.update1d()
            self.display_control_options()
        else:
            print('key press not recognized:', event.key)
        #plt.savefig('tmp1111112223333.pdf')

    def slider_vals_available(self):
        return True # Overload this if you want the selection cursor to skip over values for which data are not available.
    
    def set_slider_index(self, kv_dict):
        """ Since we store indices rather than the values (maybe silly), this is used to set those indices using values.
        The values need to be in the approved list of discrete values.
        I think this is a one-time setup method called when creating sliders.
        """
        for k,sv in kv_dict.items():
            assert k in self.slider_index
            if sv in self.discrete_vals[k]:
                self.slider_index[k] = self.discrete_val_index[k][sv]
            else:
                nk = min(myList, key=lambda x:abs(x-myNumber))
                print("  Whoops! you asked for {}=={}, but that' snot available. Choosing {} instead.".format())
                foi
                """ report and find nearest neighboru"""

    def get_current_slider_values(self):
        """ Since we store indices rather than the values (maybe silly), this is used to get a dict of the slider values 
        """
        return dict([(k,self.discrete_vals[k][sv])   for k,sv in self.slider_index.items()])

    
    def nearest_df(self,skip_xvar=True):
        """ This gets the nearest values in the self.bias dataframe. It does not also look for the nearest posteriors.
        """
        SV= self.get_current_slider_values() #[(k, self.discrete_vals[k][ii])  for k,ii in self.slider_index.items() ]
        sclosevals= ' and '.join([ '{}=={}'.format(k, v) for k,v in SV.items() if k != self.xvar or not skip_xvar])
        return # not written
        nd=self.bias.query(sclosevals).sort_values(self.xvar)
        #def maybecolorize(s):
        # multiline text in matplotlib with different colors
        # https://stackoverflow.com/questions/8391110/multiline-text-in-matplotlib-with-different-styles

        self.draw_caption()
        if 0:
          if self.axbyname['captions']:
            self.to_del += [
            bottomrighttext(self.axbyname['captions'], 'Parameters:\n'+ '\n'.join([ '  {}: {}'.format(maybecolorize(self.longnames.get(k,k)),
                                                                                                      dv if not k=='N' else {20000:'20k', 10000:'10k', 5000:'5k'}.get(int(dv),int(dv)))  for k,dv in SV.items() if k != self.xvar and not (k=='N' and self.use_best_N)]+[r'  $\beta_S^h=\beta_S^I=2$']))
        ]
        return nd

    def draw_caption(self, with_color=True):
        if self.axbyname['captions']:
            lines = [ '  {}: {}'.format(self.longnames.get(k,k),
                dv if not k=='N' else {20000:'20k', 10000:'10k', 5000:'5k'}.get(int(dv),int(dv)))  for k,dv in SV.items() if k != self.xvar and not (k=='N' and self.use_best_N)]+[r'  $\beta_S^h=\beta_S^I=2$']
            list_of_textprops = [dict(color='k' if ii != self.active_index else 'g') for ii in range(len(lines))]
            self.deleteart(self.caption_object)
            self.caption_object=  self.multicolor_text_with_border(.5,.5, lines,  list_of_textprops, ax=None,anchorpad=0, v_or_h='vertical', sep=5, box_alignment=(.5,.5))
            self.fig.canvas.draw_idle()

            #bottomrighttext(self.axbyname['captions'], 'Parameters:\n'+ '\n'.join([ '  {}: {}'.format(maybecolorize(self.longnames.get(k,k)), dv if not k=='N' else {20000:'20k', 10000:'10k', 5000:'5k'}.get(int(dv),int(dv)))  for k,dv in SV.items() if k != self.xvar and not (k=='N' and self.use_best_N)]+[r'  $\beta_S^h=\beta_S^I=2$']))
        
        
            
    def cycle_over_all_available_values(self, slider, exceptions=None):
        """ A bit obscure, but if you want gray traces to show for all possible values of slider "slider" (wihch should be in the non-X var sliders), you can call this.
        It returns to the original value when done.
        """
        current = self.get_current_slider_values()
        for csv in self.discrete_vals[slider]:
            if exceptions and csv in exceptions: continue
            self.set_slider_index({slider: csv})
            if not self.slider_vals_available(): continue
            self.update0d() # Needed before update1d
            self.update1d()
        self.set_slider_index({slider: current[slider]})
        self.update0d()
        self.update1d()
        return [x for x in self.discrete_vals[slider] if exceptions is None or csv not in exceptions]
    
    @staticmethod
    def deleteart(h):
        if h is None: return
        if not isinstance(h,list):
            h=[h]
        for ah in h:
            plt.setp(h, 'visible', False)
            """
            try:
                #ah.remove()
                #ah.set_visible(False) # Agh! This just means I can't tell whether del is working properly.
                plt.setp(h, 'visible', False)
            except AttributeError as e:
                print(e)
                pass # Error bar containers don't have it?
            """
            try:
                del ah
            except ValueError as e:
                print('Failing to remove {} '.format(ah))

    @staticmethod
    def _stack_matplotlib_text(ax,x,y,ls,lc,transform=None, mode=None,**kw):
        """
        Take a list of strings ``ls`` and colors ``lc`` and place them one under the other, with text ls[i] being shown in color lc[i].
        Adapted from https://stackoverflow.com/questions/9169052/partial-coloring-of-text-in-matplotlib
        
        OMD: In 2023 I rewrote this as multicolor_text_with_border :( :( :(
        """
        from matplotlib import transforms        
        t = ax.transData if transform is None else transform
        fig = plt.gcf()

        if mode=='stacked':
            for s,c in zip(ls[::-1],lc[::-1]):
                text = ax.text(x,y,s+" ",color=c, transform=t,
                    va='bottom',**kw)
                text.draw(fig.canvas.get_renderer())
                ex = text.get_window_extent()
                t = transforms.offset_copy(text._transform, y=ex.height, units='dots')
        
        if mode in ['sidebyside',None]:
            #horizontal version
            for s,c in zip(ls,lc):
                text = ax.text(x,y,s+" ",color=c, transform=t, **kw)
                text.draw(fig.canvas.get_renderer())
                ex = text.get_window_extent()
                t = transforms.offset_copy(text._transform, x=ex.width, units='dots')

        if mode=='vertical':
            #vertical version
            for s,c in zip(ls,lc):
                text = ax.text(x,y,s+" ",color=c, transform=t,
                        rotation=90,va='bottom',ha='center',**kw)
                text.draw(fig.canvas.get_renderer())
                ex = text.get_window_extent()
                t = transforms.offset_copy(text._transform, y=ex.height, units='dots')
                
    @staticmethod
    def multicolor_text_with_border(x,y, list_of_strings,  list_of_textprops, ax=None,anchorpad=0, v_or_h='vertical', sep=5, box_alignment=(0., 1), frameon=True ):# **kw):
        """this function creates a textbox with multiple colors: one per line.
          It could be modified to concatenate horizontally instead. (v_or_h)

        ax specifies the axes object where the labels should be drawn
        list_of_strings is a list of all of the text items
        list_if_colors is a corresponding list of colors for the strings
        axis='x', 'y', or 'both' and specifies which label(s) should be drawn"""
        from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, VPacker
        from matplotlib.offsetbox import AnnotationBbox
        if ax is None: ax=plt.gca()


        ybox = VPacker(children=[TextArea(TT, textprops=TP) for TT,TP in zip(list_of_strings,list_of_textprops)], align="left", pad=0, sep=sep)
        #ybox.set_position((x,y))
        # add border around text box
        anchored_ybox = AnnotationBbox(ybox, (x, y), xycoords='data', frameon=frameon, box_alignment=box_alignment,)#  boxcoords="offset points", bboxprops=dict(edgecolor='black' if border else 'none'))
        ax.add_artist(anchored_ybox)
        return anchored_ybox
        # This is copied from cpblUtiltiies. Latest version is there. Comments are there.

    @staticmethod
    def _massage_errorbar_args(x,y,err):
        # Having trouble with dimensionality of err. So pre-check and massage here. Only problem so far is when there's just one point to plot.
        if isinstance(x,float) or isinstance(x,int) or len(x)==1:
            err= err.reshape(2,1)
            #print('x,y,yerr:', x,y,err)
            return x,y,err
        return x,y,err

    
    def plot_1d_range(self, ):#dfc, keep_shadows=True):
        """ Passing dfc here is no longer entirely sensible, since self.data elements are more split up. We end up extracting the hashnames and using those to slice other DFs (for mean bias and FVRI). that's okay, but unpretty.
        """

    def shadowify_lines(self):
        plt.setp(self.to_gray, 'color',[.8,.8,.8], 'zorder', -10)
        for h in self.to_del:
            self.deleteart(h)#:h.remove()
        return
        for i in [0,1]:
            for j in [0,1]:
                lines = self.axs[i][j].get_lines()
                plt.setp(lines, 'color',[.8,.8,.8])
                if 0 and len(lines)>30: # Now, let's clean up a bit so we don't make too many: https://stackoverflow.com/questions/4981815/how-to-remove-lines-in-a-matplotlib-plot
                    l = lines.pop(0)
                    self.deleteart(l)#l.remove()

        
    def remove_markers(self):
        plt.setp( plt.findobj(plt.gcf(), lambda x: plt.getp(x ,'label')=='tmp'),
                  'visible', False)
        return # Why does this cause so much trouble
        for h in self.to_del:
            self.deleteart(h)#:h.remove()
            #del h
    def plot_0d_plots(self, ):#adf):
        """  Plot the SWL distribution.  Also, add red squares on some 1-D plots to show where we are.
        """
        

        
    def update1d(self):#,val):
        #nd =self.nearest_df()
        plt.setp(self.to_del, 'visible',False)
        self.plot_1d_range()#nd)
        #self.plot_Stan(nd)#.dropna())
        self.plot_0d_plots()#self.nearest_df(skip_xvar=False))
        #self.set_ax_lims()        
        self.fig.canvas.draw_idle()
        self.draw_caption()

    def update0d(self):#,val):
        nd=self.nearest_df(skip_xvar=False)
        self.plot_0d_plots()#nd)
        self.fig.canvas.draw_idle()


    def static_plot(self): # What is this?
        self.update1d() # updates 0d too.

        return 

