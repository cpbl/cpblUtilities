#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .cpblUtilities_config import defaults,update_paths
import os
import re


def google_api_spreadsheet_to_pandas(credentials_json_file_or_dict, spreadsheetname, tabname):
    """
    Follow the directions here and linked here: https://github.com/burnash/gspread
    You'll need to share your doc with the strange email address in your credentials file.
    """
    import gspread # Installed with pip!  "pip install --user gspread "
    from oauth2client.service_account import ServiceAccountCredentials # Installed with pip!  "pip install --user oauth2client"
    scope = ['https://spreadsheets.google.com/feeds']
    if isinstance(credentials_json_file_or_dict,dict):
        credentials = ServiceAccountCredentials.from_json_keyfile_dict(credentials_json_file_or_dict, scope)
    else:
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_json_file_or_dict, scope)
        
    gc = gspread.authorize(credentials)
    wks = gc.open(spreadsheetname).worksheet(tabname)
    df=pd.DataFrame(wks.get_all_values())
    df.columns=df.iloc[0,:]
    df=df.iloc[1:]
    return(df)


def downloadOpenGoogleDoc(url, filename=None, fileformat=None, pandas=False, update=True,
                          sheet_name=None):
    """
    If you: Set a Google doc or Google drive document to be readable (avoid editable, do avoid downloading comments) to anyone with the link:

    Then this function will return a version of it from online, if networked, or the latest download, if not.

    The filename (currently mandatory; should be fixed to be a hash of the url by default) should not include a path.  (TO DO / Not done)

    Specify fileformat as one of "txt", "ods", "xlsx", "odt" or etc as supported by Google

    This returns the full-path filename of the downloaded file, unless pandas is True for spreadsheets, in which case it returns a pandas DataFrame (?)

    If update False, and the file has already been downloaded, download will be skipped.

    N.B.: For txt file download, File should be set to anyone with the link can view, but not comment. Otherwise comments will also be downloaded to a text file.

    """
    from  xlrd import XLRDError
    if pandas:
        import pandas as pd
        #assert fileformat in ['xlsx']

    if not url.endswith('/'): url=url+'/'
    if fileformat is None:
        fileformat='ods' #'xlsx'

    full_file_name        =(defaults['paths']['scratch'])*(not os.path.split(filename)[0])  +filename
    if update or not os.path.exists(full_file_name):
        oss=' wget "'+url+'export?format='+fileformat+'" -O '+full_file_name+'-dl'
        print(oss)
        result=os.system(oss)
        if result:
            print('  NO INTERNET CONNECTION, or other problem ({}) grabbing Google Docs file. Using old offline version ({})...'.format(result,filename))
        else:
            if fileformat in ['txt']:
                result = os.system('dos2unix -n '+ full_file_name+'-dl ' + full_file_name)
            else:
                result=os.system(' mv '+ full_file_name+'-dl ' + full_file_name)
                if 0:   # This doesn't work anymore. read() not willing to read a binary file if it doesn't look like utf8. So need more try/except here
                    checkt = open(full_file_name, 'rb').read()
                    if checkt.startswith('<!DOCTYPE html>'):
                        raise TypeError("Downloaded file arrived as HTML; this means something's wrong")
            print(' ... Downloaded and wrote {}'.format(full_file_name))
            assert not result
    else:
        print(('   Using local (offline) version of '+filename))
    if pandas and fileformat in ['xlsx']:
        return(   pd.read_excel(full_file_name, sheet_name=sheet_name))
    if pandas and fileformat in ['ods']:
        return(   pd.read_excel(full_file_name,  engine='odf', sheet_name=sheet_name))


    return(full_file_name)

