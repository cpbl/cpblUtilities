#!/usr/bin/env python

import matplotlib.pyplot as plt
import sys
import numpy as np
import os
import pandas as pd



def prepare_figure_for_publication(ax=None,
        width_cm=None,
        width_inches=None,
                                   height_cm=None,
        height_inches=None,
                                   fontsize=None,
        fontsize_labels=None,
        fontsize_ticklabels=None,
        fontsize_legend=None,
                                   fontsize_annotations =None,
                                   TeX = True, # Used for ax=None case (setup)
        ):

    """
    Two ways to use this:
    (1) Before creating a figure, with ax=None
    (2) To fine-tune a figure, using ax

    One reasonable option for making compact figures like for Science/Nature is to create everything at double scale. 
    This works a little more naturally with Matplotlib's default line/axis/etc sizes.

    Also, if you change sizes of, e.g. xticklabels and x-axis labels after they've been created, they will not necessarily be relocated appropriately.
    So  you can call prepare_figure_for_publication with no ax/fig argument to set up figure defaults
    prior to creating the figure in the first place.


Some wisdom on graphics:
 - 2015: How to produce PDFs of a given width, with chosen font size, etc:
   (1) Fix width to journal specifications from the beginning / early. Adjust height as you go, according to preferences for aspect ratio:
    figure(figsize=(11.4/2.54, chosen height))
   (2) Do not use 'bbox_inches="tight"' in savefig('fn.pdf').  Instead, use the subplot_adjust options to manually adjust edges to get the figure content to fit in the PDF output (really? No! use command line pdfcrop)
   (3) Be satisfied with that. If you must get something exactly tight and exactly the right size, you do this in Inkscape. But you cannot scale the content and bbox in the same step. Load PDF, select all, choose the units in the box at the top of the main menu bar, click on the lock htere, set the width.  Then, in File Properties dialog, resize file to content. Save.

    """
    if ax is None: # Set up plot settings, prior to creation fo a figure
        params = { 'axes.labelsize': fontsize_labels if fontsize_labels is not None else fontsize,
                   'font.size': fontsize,
                   'legend.fontsize': fontsize_legend if fontsize_legend is not None else fontsize,
                   'xtick.labelsize': fontsize_ticklabels if fontsize_ticklabels is not None else fontsize_labels if fontsize_labels is not None else fontsize,
                   'ytick.labelsize': fontsize_ticklabels if fontsize_ticklabels is not None else fontsize_labels if fontsize_labels is not None else fontsize,
                   'figure.figsize': (width_inches, height_inches),
            }
        if TeX:
            params.update({
                   'text.usetex': TeX,
                       'text.latex.preamble': r'\usepackage{amsmath}\usepackage{amssymb}',
                #'text.latex.unicode': True,
            })
        if not TeX:
            params.update({'text.latex.preamble':''})

        plt.rcParams.update(params)
        return
    
    fig = ax.get_figure() 
    if width_inches:
        fig.set_figwidth(width_inches)
        assert width_cm is None
    if height_inches:
        fig.set_figheight(height_inches)
        assert height_cm is None
    if width_cm:
        fig.set_figwidth(width_cm/2.54)
        assert width_inches is None
    if height_cm:
        fig.set_figheight(height_cm/2.54)
        assert height_inches is None
    
    #ax = plt.subplot(111, xlabel='x', ylabel='y', title='title')
    for item in fig.findobj(plt.Text) + [ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels():
        if fontsize:
            item.set_fontsize(fontsize)

def unique_everseen(seq, key=None):
    seen = set()
    seen_add = seen.add
    return [x for x, k in zip(seq, key) if not (k in seen or seen_add(k))]


def reorderLegend(ax=None, order=None, key=None, unique=False, **legend_args):
    """
    Returns tuple of legend handle, artist handles, labels for axis ax, after reordering them to conform to the label order `order`, and if unique is True, after removing entries with duplicate labels.
    Backwards incompatibliity 2020!: needs to return handle to new legend, too, since old legend gets removed (you can reverse that with ax.add_artist(handle)


    Examples:

    h,_,_ = reorderLegend()   # Sort existing legend alphabetically

    h,_,_ = reorderLegend(order= ['top', 'middle', 'bottom']) # If any of those exist as labels, put them first, in given order


    Issues:
    What if I have more than one legend created /associated with the axis! Need to be able to specify legend handle?

    """
    if ax is None:
        ax = plt.gca()

    handles, labels = ax.get_legend_handles_labels()
    # sort both labels and handles by labels
    labels, handles = list(zip(*sorted(zip(labels, handles), key=lambda t: t[0])))
    # Sort according to a given list (not necessarily complete)
    if order is not None:
        keys = dict(list(zip(order, list(range(len(order))))))
        labels, handles = list(zip(
            *sorted(zip(labels, handles), key=lambda t, keys=keys: keys.get(t[0], np.inf))))
    if unique:  # Keep only the first of each handle
        #from  more_itertools import unique_everseen   # Use this instead??
        labels, handles = list(zip(
            *unique_everseen(list(zip(labels, handles)), key=labels)))
    H=ax.legend(handles, labels, **legend_args)

    return(H,handles, labels)

            
def plot_diagonal(xdata=None, ydata=None, ax=None, **args):
    """ Plot a 45-degree line 
    """
    if ax is None: ax = plt.gca()
    #LL = min(min(df[xv]), min(df[yv])), max(max(df[xv]), max(df[yv]))
    if xdata is None and ydata is None:
        xl, yl = ax.get_xlim(), ax.get_ylim()
        LL = max(min(xl), min(yl)),    min(max(xl), max(yl)),
    elif xdata is not None and ydata is None:
        assert isinstance(xdata, pd.DataFrame)
        dd = xdata.dropna()
        LL = dd.min().max(), dd.max().min()
    else:
        assert xdata is not None
        assert ydata is not None
        #if isinstance(xdata, pd.Series): xdata = xdata.vlu
        xl, yl = xdata, ydata
        LL = max(min(xl), min(yl)),    min(max(xl), max(yl)),
    ax.plot(LL, LL,  **args)

                        
def figureFontSetup(uniform=12,figsize='paper', amsmath=True):
    """
This is deprecated. Use prepare_figure_for_publication


Set font size settings for matplotlib figures so that they are reasonable for exporting to PDF to use in publications / presentations..... [different!]
If not for paper, this is not yet useful.


Here are some good sizes for paper:
    figure(468,figsize=(4.6,2)) # in inches
    figureFontSetup(uniform=12) # 12 pt font

    for a subplot(211)

or for a single plot (?)
figure(127,figsize=(4.6,4)) # in inches.  Only works if figure is not open from last run!
        
why does the following not work to deal with the bad bounding-box size problem?!
inkscape -f GSSseries-happyLife-QC-bw.pdf --verb=FitCanvasToDrawing -A tmp.pdf .: Due to inkscape cli sucks! bug.
--> See savefigall for an inkscape implementation.

2012 May: new matplotlib has tight_layout(). But it rejigs all subplots etc.  My inkscape solution is much better, since it doesn't change the layout. Hoewever, it does mean that the original size is not respected! ... Still, my favourite way from now on to make figures is to append the font size setting to the name, ie to make one for a given intended final size, and to do no resaling in LaTeX.  Use tight_layout() if it looks okay, but the inkscape solution in general.
n.b.  a clf() erases size settings on a figure! 

    """
    print(' Caution: in 2021 this figureFontSetup is causing postcript font trouble. Try NOT using this. :)  ')
    whereIsThisMessageComingFrom_tmpdebug
    figsizelookup={'paper':(4.6,4),'quarter':(1.25,1) ,None:None}
    try:
        figsize=figsizelookup[figsize]
    except KeyError as TypeError:
        pass
    params = {#'backend': 'ps',
           'axes.labelsize': 16,
        #'text.fontsize': 14,
        'font.size': 14,
           'legend.fontsize': 10,
           'xtick.labelsize': 16,
           'ytick.labelsize': 16,
           'text.usetex': True,
           'figure.figsize': figsize
        }
           #'figure.figsize': fig_size}
    if uniform is not None:
        assert isinstance(uniform,int)
        params = {#'backend': 'ps',
           'axes.labelsize': uniform,
            #'text.fontsize': uniform,
           'font.size': uniform,
           'legend.fontsize': uniform,
           'xtick.labelsize': uniform,
           'ytick.labelsize': uniform,
           'text.usetex': True,
            #'text.latex.unicode': True,
            'text.latex.preamble':r'\usepackage{amsmath}\usepackage{amssymb}',
           'figure.figsize': figsize
           }
        if not amsmath:
            params.update({'text.latex.preamble':''})

    plt.rcParams.update(params)
    #plt.rcParams['text.latex.unicode']=True
    #if figsize:
    #    plt.rcParams[figure.figsize]={'paper':(4.6,4)}[figsize]

    return(params)

# Nothing fancy here, but reminder how to use the "ma" keyword and xycoords:
def bottomlefttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, frameon=True, edgecolor = "0.5", **args):
    return  ax.annotate(text, xy=(dx, dy), xycoords = 'axes fraction', ha='left', ma='left', va='bottom', color=color, size=size,     bbox= None if not frameon else dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha), **args)
def bottomrighttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, frameon=True, edgecolor = "0.5", **args):
    return  ax.annotate(text, xy=(1-dx, dy), xycoords = 'axes fraction', ha='right', ma='left', va='bottom', color=color, size=size,  bbox= None if not frameon else dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha), **args)
def toplefttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, frameon=True, edgecolor = "0.5", **args):
    return  ax.annotate(text, xy=(dx, 1-dy), xycoords = 'axes fraction', ha='left', ma='left', va='top', color=color, size=size,  bbox= None if not frameon else dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha), **args)
def toprighttext(ax,text, size=12, alpha = 0.5, color='k', dx=1.0/20, dy=1.0/20, frameon=True, edgecolor = "0.5", **args):
    return  ax.annotate(text, xy=(1-dx, 1-dy), xycoords = 'axes fraction', ha='right', ma='left', va='top', color=color, size=size,   bbox= None if not frameon else dict(boxstyle="round", fc="w", ec=edgecolor, alpha=alpha), **args)

def vtext(xvec,  yvec, tvec,
          tname=None, # For second calling format only
          ax=None, **args):
    """ Why does ax.text only take scalars?
 This is a vectorized version.
    Either pass Series (or lists),
    vtext(x, y, texts, ax=ax, **args)    
    use alternative calling form, in which you pass a DataFrame and column names:
    vtext(df, xname, yname, tname, ax=ax, **args)
"""
    
    # Choose between calling versions:
    if isinstance(xvec, pd.DataFrame):
        assert isinstance(yvec, str)
        assert isinstance(tvec, str)
        assert isinstance(tname, str)
        df=xvec
        xvec = df[yvec].values
        yvec = df[tvec].values
        tvec = df[tname].values
    else:
        assert tname is None
        # Still to do: convert from Series, etc:
        xvec=xvec.values
        yvec=yvec.values
        tvec=tvec.values
        
    assert len(xvec)== len(yvec) ==len(tvec)
    cvec = None
    if 'color' in args and args['color'] is not None:
        if len(args['color']) == len(xvec):
               cvec = args.pop('color')
        elif isinstance(args['color'], str) and args['color'] in df.columns:
               cvec = df[args.pop('color')].values

    hh=[]             
    for ii,tt in enumerate(tvec):
        if cvec is None:
            hh+= [ ax.text(xvec[ii], yvec[ii], tt, **args) ]
            
        else:
            hh+= [ ax.text(xvec[ii], yvec[ii], tt, color = cvec[ii], **args)]

    return  hh

def test_vtext(): # 2024-08: ax.text is broken: N.B.: If x,y coordinates are beyond axes limits at the time you call text(), it will never show up.
    return

def test_multicolor_text():
    plt.close('all')
    fig,ax=plt.subplots(1)
    ax.plot([1,2,3],[1,2,3])
    hh= multicolor_text(1,2, ['a','bb','ccc'], [dict(color='r'),dict(color='g'),dict(color='b')], ax=ax)
    plt.show()

def _stack_matplotlib_text(ax,x,y,ls,lc,transform=None, mode=None,**kw):
    # AHHHHHHHHHH This effort was duplicated later by the function below!
    """
    Take a list of strings ``ls`` and colors ``lc`` and place them one under the other, with text ls[i] being shown in color lc[i].
    Adapted from https://stackoverflow.com/questions/9169052/partial-coloring-of-text-in-matplotlib

    OMD: In 2023 I rewrote this as multicolor_text :( :( :(
    """
    from matplotlib import transforms        
    t = ax.transData if transform is None else transform
    fig = plt.gcf()

    if mode=='stacked':
        for s,c in zip(ls[::-1],lc[::-1]):
            text = ax.text(x,y,s+" ",color=c, transform=t,
                va='bottom',**kw)
            text.draw(fig.canvas.get_renderer())
            ex = text.get_window_extent()
            t = transforms.offset_copy(text._transform, y=ex.height, units='dots')

    if mode in ['sidebyside',None]:
        #horizontal version
        for s,c in zip(ls,lc):
            text = ax.text(x,y,s+" ",color=c, transform=t, **kw)
            text.draw(fig.canvas.get_renderer())
            ex = text.get_window_extent()
            t = transforms.offset_copy(text._transform, x=ex.width, units='dots')

    if mode=='vertical':
        #vertical version
        for s,c in zip(ls,lc):
            text = ax.text(x,y,s+" ",color=c, transform=t,
                    rotation=90,va='bottom',ha='center',**kw)
            text.draw(fig.canvas.get_renderer())
            ex = text.get_window_extent()
            t = transforms.offset_copy(text._transform, y=ex.height, units='dots')


def multicolor_text_with_border(x,y, list_of_strings,  list_of_textprops, ax=None,anchorpad=0, v_or_h='vertical', sep=5, box_alignment=(0., 1), frameon=True ):# **kw):
    # AHHHHHHHHHH This effort duplicated the functiona bove!            
    """this function creates a textbox with multiple colors: one per line.
      It could be modified to concatenate horizontally instead. (v_or_h)

    ax specifies the axes object where the labels should be drawn
    list_of_strings is a list of all of the text items
    list_if_colors is a corresponding list of colors for the strings
    axis='x', 'y', or 'both' and specifies which label(s) should be drawn"""
    from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, VPacker
    from matplotlib.offsetbox import AnnotationBbox
    if ax is None: ax=plt.gca()


    ybox = VPacker(children=[TextArea(TT, textprops=TP) for TT,TP in zip(list_of_strings,list_of_textprops)], align="left", pad=0, sep=sep)
    #ybox.set_position((x,y))
    # add border around text box
    anchored_ybox = AnnotationBbox(ybox, (x, y), xycoords='data', frameon=frameon, box_alignment=box_alignment,)#  boxcoords="offset points", bboxprops=dict(edgecolor='black' if border else 'none'))
    ax.add_artist(anchored_ybox)
    return anchored_ybox
    # This is copied from cpblUtiltiies. Latest version is there. Comments are there.

            
def savefig_pdfcrop(savefile, lofs=None, fig=None, pdf=True, png=False, pdfcrop=True, **args):
    """
    Use this for all figure saving. Then at end of module:
    from cpblUtilities import mergePDFs
    if lofs:
        mergePDFs(lofs, outFn= paths['graphics']+'tmp_allfigures_'+runmode+'.pdf', allow_not_exist=True, delete_originals = False, forceUpdate=False,
                  crop=False, # trim each PDF to its bounding box (in place!)
                  with_titles=True, # Insert a minipage containing the title of each PDF before each PDF
                  launch = True, # Launch evince viewer of the result
                  )

    """
    if savefile.lower().endswith('.pdf'):
        fnpdf=savefile
    elif savefile.lower().endswith('.png'):
        fnpdf=savefile[:-3]+'.pdf'
    else:
        fnpdf=savefile+'.pdf'
    if fig:
        fig.savefig(fnpdf, **args)
    else:
        plt.savefig(fnpdf, **args)
    if lofs is not None: # Add the plot name to a provided list pointer
        lofs+= [fnpdf]
    if pdfcrop and pdf and fnpdf.lower().endswith('.pdf'):
        os.system('pdfcrop {f} {f}&'.format(f=fnpdf))
              


def test_bug3_for_multipage_plot_iterator():
    import numpy as np
    import matplotlib.pyplot as plt

    def prep_figure():
        plt.close('all')
        fig, axs = plt.subplots(4, 3, figsize=(11, 8.5))
        axs = np.concatenate(axs)
        for ii in range(5):
            axs[ii].plot([1, 2, 3], [-10, -1, -10])
            axs[ii].set_ylabel('ylabel')
            axs[ii].set_xlabel('xlabel')
        return fig, axs

    fig, axs = prep_figure()
    plt.tight_layout()
    plt.show()
    plt.savefig('tmp.pdf', )

    # Try deleting extra axes
    fig, axs = prep_figure()
    for ii in range(5, 12):
        fig.delaxes(axs[ii])
    plt.tight_layout()
    plt.draw()
    plt.savefig('tmpd.pdf', )

    # Try hiding  extra axes
    fig, axs = prep_figure()
    for ii in range(5, 12):
        axs[ii].set_visible(False)
    plt.tight_layout()
    plt.draw()
    plt.savefig('tmph.pdf', )


def multipage_plot_iterator(items, nrows=None, ncols=None, filename=None, wh_inches=None, transparent=True,):
    """
    If you want to have a series of subplots that goes more than one page,  use this to generate figs and axes handles.
    You specify the list of data items which you will use to plot in each axis, how many (rows and columns) to plot per page, and the filename stem for the pages (which will ultimately be a single multi-page PDF).
    In addition, you can specify some other stuff about the layout.
    Then this iterator will return series of item, fig and axis values for each item in the list (e.g data for one country per axis), as well as whether each axis is on the right/left/bottom of the page.

    In order to be able to finish up by concatenating the PDFs, etc, one extra "yield" will be made. This simply duplicates the final axis/plot. Therefore, do not use your own counters in loops over this generator. Instead, build any counters into the
    "items" data structure.

    items could be an iterator itself, but that is not implemented yet. It must be a list at the moment.

    transparent [True]:  Set this to False if you use  ax.set_facecolor in your loop; this will avoid using transparency in the saved result.

    Deletes the one-page files after creating the final merged-page produce

    To do:
     - check that this also works nicely for ncols==nrows==1
     - Rewrite so "items" can be a groupby object or other iterable.
    """
    from .utilities import str2pathname, mergePDFs
    if str2pathname(filename, check=True, includes_path=True):
        print((' WARNING ({}):  Modifying filename to clean out certain characters'.format(
            sys._getframe().f_code.co_name)))
        fn0 = filename
        # Proceed anyway.
        filename = str2pathname(filename, includes_path=True)
        print(('   FROM {} \n    --> {}'.format(fn0, filename)))
    nItems = len(items)
    if wh_inches is None:
        wh_inches = [8.75, 7]  # for full-page figures
    elif wh_inches == "landscape":
        wh_inches = [8.75, 7]  # for full-page figures
    elif wh_inches == "portrait":
        wh_inches = [7, 8.75]  # for full-page figures
    elif wh_inches == "beamer": # old school beamer
        wh_inches = [8.75, 7*2/3]  
    elif wh_inches == "beamer-16:9": # newer, wide-format beamer
        wh_inches = [10, 10*9/16]  
    ncols = 4 if ncols is None else ncols
    nrows = 3 if nrows is None else nrows
    splotNums = [(ii*nrows*ncols, min(nItems, (ii+1)*nrows*ncols))
                 for ii in range(int(np.ceil(nItems/nrows/ncols)))]
    nPages = len(splotNums)
    #figureFontSetup(uniform=9)
    print(" You should probably set the font to be uniform=9 in mpl setup? But I'm removing the call due to my warning 2021.")
    pagefiles = []
    for ipage, (ssplot, esplot) in enumerate(splotNums):
        if ipage % 10 == 0:
            plt.close('all')
        # If it's the last page, possibly adjust figure height:
        if 0:
            # less than nrows when the page isn't full
            actualRows = int(np.ceil((esplot-ssplot-1)*1.0/ncols))
            print(wh_inches)
            wh_inches[1] = wh_inches[1] * actualRows/nrows
            print(wh_inches)
        else:
            actualRows = nrows

        fig, axs = plt.subplots(actualRows, ncols, figsize=wh_inches)#[::-1])  # Why was this [::-1] ???
        try:  # Normally, there is more than one axis:
            axs = list(np.concatenate(axs))
        except TypeError as err:
            axs = [axs]
        # If we don't need them all, erase some. This allows for us to call layout_tight() later, though so far it doesn't drop the whitespace.
        for idelAx in np.arange(esplot-ssplot, len(axs)):
            axs[idelAx].plot(1, 1)
            # fig.delaxes(axs[idelAx])
            # axs[idelAx].set_visible(False) # This is not the same as deleting them
        for iItem, anitem in enumerate(items[ssplot:esplot]):
            ax = axs[iItem]

            yield(dict(data=anitem, ax=ax, fig=fig, bottom=iItem > esplot-ssplot-ncols-1,  # iItem>=(nrows-1)*ncols ,
                       left=not (iItem) % ncols, first=iItem == 0, last=iItem == esplot-ssplot, ipage=ipage))

            # yield(dict(data = anitem, ax = ax, fig = fig, bottom = iItem > erow-srow-ncols-1, #iItem>=(nrows-1)*ncols ,
            #           left = not (iItem)%ncols, first = iItem==0, last = iItem == erow-srow-1 , ipage =ipage))

        plt.tight_layout()
        for idelAx in np.arange(esplot-ssplot, len(axs)):
            # fig.delaxes(axs[idelAx])
            axs[idelAx].set_visible(False)
        pagefilename = filename+'page%02d' % ipage
        pagefiles += [pagefilename+'.pdf']
        plt.savefig(pagefilename+'.pdf', )# wh_inches=wh_inches,)
        # It seems something in the following is no good (for grid cell images) so just use plain savefig
        #savefigall(pagefilename,  wh_inches=wh_inches,
        #           rv=False, png=False, transparent=transparent)
    mergePDFs(pagefiles, filename+'ALL.pdf')
    # This allows a final "next" by the caller to finish the final saving.
    yield (dict(data=anitem, ax=ax, fig=fig, bottom=iItem >= (actualRows-1)*ncols, left=not (iItem) % ncols, first=iItem == 0, last=iItem == esplot-ssplot, ipage=ipage))
    # yield (dict(data = anitem, ax = ax, fig = fig, bottom = iItem>=(nrows-1)*ncols, left = not (iItem)%ncols, first = iItem==0, last = iItem == erow-srow-1 , ipage =ipage)) # This allows a final "next" by the caller to finish the final saving.


def histogram_of_blocks_each_observation_labelled(df, value_col=None, label_col=None, nbins=10, ax=None, **kwargs):
    """
    """
    import pandas as pd
    import matplotlib.pyplot as plt
    import numpy as np 

    if isinstance(df,str) and df =='test':
        nbins=10
        df = pd.DataFrame({'x': np.random.random(100),
                         'label': [str(x)  for x in range(100)]}
                         )
        histogram_of_blocks_each_observation_labelled(df, 'x', 'label', nbins=nbins)
        return

    # Get hist:
    counts,bins=np.histogram(df[value_col], bins=nbins)
    df['bincut'] = pd.cut(df[value_col], bins=bins)

    if ax is None:
        fig,ax = plt.subplots()
    
    for abin,adf in df.groupby('bincut'):
        bottom=0
        for label in adf[label_col].sort_values(ascending=False):
            ax.bar(abin.mid, 1, width=abin.length*.9, bottom=bottom, color=np.array([1,1,1])*.7)
            ax.text(abin.mid, bottom+.5, label, ha='center', va='center')
            bottom+=1

    return ax                                              
    
        
if __name__ == '__main__':
    test_multicolor_text()
